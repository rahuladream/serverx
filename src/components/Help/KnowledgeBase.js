import React from 'react';
import Menu from '../Layout/Menu';
import Footer from '../Layout/Footer';

class KnowledgeBase extends React.Component {
    render(){
        return(
            <body>
            <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-headerv2">
               <div className="search-header-block">
                   <form id="pitursrach-header" name="formsearch" method="get" action="#">
                       <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                       <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                       <a className="closesrch-her-block np-dsp-block">
                           <span className="first-stright"></span>
                           <span className="second-stright"></span>
                       </a>
                   </form>
               </div>
               <div className="header-animation">
                   <div id="particles-bg"></div>
               </div>
               <Menu />
               <div className="mt-auto"></div>
           </div>
        
	<section class="padding-100-0">
	<div class="container">
	<div class="row">
    <div class="col-md-12 text-center about-us-page-title">
    <h6 class="about-us-page-title-title"> You got a question ? </h6>
    <h2 class="about-us-page-title-sub-title">We may have already answered your question<br/>you can look down on your question.</h2>
    <p class="about-us-page-title-text"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	</div>
    </div>


	<div class="row question-area-page justify-content-left mr-tp-120">
	<div class="col-md-4 no-phone-display">
	<div class="question-area-answer-navs">
	<div class="nuhost-filter-container"> {/* start q&a filter */}
	<i class="fas fa-search"></i>
	<input type="text" id="nuhost-filter-input" onkeyup="FilterListSection()" placeholder="Search for quastions here.."/> {/* q&a filter input */}
    </div>
	<div class="nuhost-filter-list-container min-height-auto"> {/* q&a filter list container */}
	<ul id="nuhost-filter-list"> {/* start q&a filter item list */}
    <li><a href="#go-to-qst-01">how can i order new host ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a href="#go-to-qst-02">what is resellers hosting ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a href="#go-to-qst-03">i want new domain name <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a href="#go-to-qst-04">can i pay with paypal ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a href="#go-to-qst-05">are this site secure ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a href="#go-to-qst-06">i want WordPress site <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a href="#go-to-qst-07">your phone number <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    </ul> {/* end q&a filter item list */}
	</div> {/* end q&a filter */}
	
	</div>
	</div>
	
	<div class="col-md-8">
	<div class="question-area-answer-banner">
	<div class="question-area-answer-banner-icon"><i data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#videomodal" class="fas fa-play-circle gris-color video-btn"></i></div>
	<div class="question-area-answer-banner-text"><h5>Nuhost team love to give support for their clients <br/> free & 24/7 days <span>just take 33 seconds on your time.</span></h5></div>
	</div>
	
	<div class="question-area-answer-body">
	<ul>
	<li id="go-to-qst-01"><span>What payment methods are available ?</span>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor.</p>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amigula Eget Dolor.</p>
	</li>
	
	<li id="go-to-qst-02"><span>What payment methods are available ?</span>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor.</p>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amigula Eget Dolor.</p>
	</li>
	
	<li id="go-to-qst-03"><span>What payment methods are available ?</span>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor.</p>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amigula Eget Dolor.</p>
	</li>
	
	
	<li id="go-to-qst-04"><span>What payment methods are available ?</span>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor.</p>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amigula Eget Dolor.</p>
	</li>
	
	
	<li id="go-to-qst-05"><span>What payment methods are available ?</span>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor.</p>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amigula Eget Dolor.</p>
	</li>
	
	
	<li id="go-to-qst-06"><span>What payment methods are available ?</span>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor.</p>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amigula Eget Dolor.</p>
	</li>
	
	
	<li id="go-to-qst-07"><span>What payment methods are available ?</span>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor.</p>
	<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Lorem Ipsum Dolor Sit Amigula Eget Dolor.</p>
	</li>
	
	
	</ul>
	
	</div>
	</div>
	
	</div>
	</div>
	
	</section>
        <Footer/>
       </body>  
        )
    }
}
export default KnowledgeBase;