import React from 'react';

class Faq extends React.Component{
    render() {
        return(
            <section class="padding-90-0-100 gray-background position-relative overflow-hidden">
            <span class="section-with-moon-back-under"></span>
            <span class="section-with-moon-men-quastions-under"></span>
            <div class="container">
            <div class="tittle-simple-one"><h5>Frequently asked questions<span>Still on doubt? Here have some top selected questions asked by our customer.</span></h5></div>
        
            <div class="row justify-content-left mr-tp-90">
            <div class="col-md-6">
            <div class="nuhost-filter-container"> {/* start q&a filter */}
            <i class="fas fa-search"></i>
            <input type="text" id="nuhost-filter-input" onkeyup="FilterListSection()" placeholder="Search for questions here.."/> {/* q&a filter input */}
            </div>
            <div class="nuhost-filter-list-container"> {/* q&a filter list container */}
            
            <div class="filter-content-box" id="go-to-qst-01-content"> {/* start q&a first content box */}
            <span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
            <span class="first-stright"></span>
            <span class="second-stright"></span>
            </span>
            <h5>how can i order new host ?</h5>
            <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div> {/* end q&a first content box */}
            
            <div class="filter-content-box" id="go-to-qst-02-content"> {/* start q&a second content box */}
            <span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
            <span class="first-stright"></span>
            <span class="second-stright"></span>
            </span>
            <h5>what is resellers hosting ?</h5>
            <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div> {/* end q&a second content box */}
            
            <div class="filter-content-box" id="go-to-qst-03-content"> {/* start q&a second content box */}
            <span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
            <span class="first-stright"></span>
            <span class="second-stright"></span>
            </span>
            <h5>what is resellers hosting ?</h5>
            <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div> {/* end q&a second content box */}
            
            <div class="filter-content-box" id="go-to-qst-04-content"> {/* start q&a second content box */}
            <span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
            <span class="first-stright"></span>
            <span class="second-stright"></span>
            </span>
            <h5>what is resellers hosting ?</h5>
            <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div> {/* end q&a second content box */}
            
            <div class="filter-content-box" id="go-to-qst-05-content"> {/* start q&a second content box */}
            <span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
            <span class="first-stright"></span>
            <span class="second-stright"></span>
            </span>
            <h5>what is resellers hosting ?</h5>
            <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div> {/* end q&a second content box */}
            
            
            <div class="filter-content-box" id="go-to-qst-06-content"> {/* start q&a second content box */}
            <span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
            <span class="first-stright"></span>
            <span class="second-stright"></span>
            </span>
            <h5>what is resellers hosting ?</h5>
            <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div> {/* end q&a second content box */}
            
            
            <div class="filter-content-box" id="go-to-qst-07-content"> {/* start q&a second content box */}
            <span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
            <span class="first-stright"></span>
            <span class="second-stright"></span>
            </span>
            <h5>what is resellers hosting ?</h5>
            <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div> {/* end q&a second content box */}
            
            <ul id="nuhost-filter-list"> {/* start q&a filter item list */}
            <li><a id="go-to-qst-01">how can i order new host ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
            <li><a id="go-to-qst-02">what is resellers hosting ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
            <li><a id="go-to-qst-03">i want new domain name <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
            <li><a id="go-to-qst-04">can i pay with paypal ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
            <li><a id="go-to-qst-05">are this site secure ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
            <li><a id="go-to-qst-06">i want WordPress site <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
            <li><a id="go-to-qst-07">I want to know how i can contact you <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
            </ul> {/* end q&a filter item list */}
            </div> {/* end q&a filter */}
            </div>
            </div>
            
            </div>
            </section>
        )
    }
}

export default Faq;