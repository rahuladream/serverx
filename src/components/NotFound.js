import React from 'react';
import Menu from './Layout/Menu';
import Footer from './Layout/Footer';

class NotFound extends React.Component {
    render(){
        return(
            <body>
            <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-headerv2">
               <div className="search-header-block">
                   <form id="pitursrach-header" name="formsearch" method="get" action="#">
                       <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                       <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                       <a className="closesrch-her-block np-dsp-block">
                           <span className="first-stright"></span>
                           <span className="second-stright"></span>
                       </a>
                   </form>
               </div>
               <div className="header-animation">
                   <div id="particles-bg"></div>
               </div>
               <Menu />
               <div className="mt-auto"></div>
           </div>
	<section className="padding-100-0 text-center">
	<div className="container">
	<div className="page-404-styles">
	<img src="img/header/404.png" alt="" />
	<h6 className="megssage-error">Ooops! sorry this page does not found <small>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetue</small></h6>
	<a className="btn-order-default-nuhost" href="#">GO HOME NOW</a>
	</div>
	</div>
	</section>
        <Footer/>
       </body>  
        )
    }
}
export default NotFound;