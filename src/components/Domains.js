import React from 'react';
import Header from './Layout/Header';
import Footer from './Layout/Footer';
import Menu from './Layout/Menu';

class Domains extends React.Component {
    render() {
        return(
            <body>
     <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-header">
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>
        </div>
        <div className="header-animation">
        <div id="particles-bg"></div>
            <span className="courve-gb-hdr-top"></span>
            <a className="support-header-ring" href="#"><img src="img/svgs/support.svg" alt="" /> <span>support team</span></a>
        </div>
        <Menu />
        <main className="inner cover header-heeadline-title mb-auto justify-content-center">
        <h5><span className="blok-on-phon">domain search</span></h5>
        <p>Register your brand name with us and get lots of discount and services</p>
        <form action="#" id="domain-search" className="not-index-domain-header-search" data-wow-delay="0.5s">
        <input id="domain-text" type="text" name="domain" value="backspaceboys.in" />
        <span className="inline-button">
        <button id="search-btn" type="submit" name="submit" value="Search"> <img src="img/svgs/search.svg" alt="search icon"/> </button>
        </span>

        <div className="domain-price-header mr-auto not-index-domain-header-prices">

        <a>
        <img src="img/domain/com.png" alt="domain"/>
        <span>₹199</span>
        </a>

        <a>
        <img src="img/domain/net.png" alt="domain"/>
        <span>₹109</span>
        </a>

        <a>
        <img src="img/domain/org.png" alt="domain"/>
        <span>₹99</span>
        </a>

        <a className="no-phon-dsply">
        <img src="img/domain/store.png" alt="domain"/>
        <span>₹899</span>
        </a>

        </div>

        </form>


        </main>
        <div className="mt-auto"></div>
    </div>
    <section class="padding-100-0-30">
	<div class="container">{/* domain table prices */}
	<h2 class="d-none">domain prices</h2>
	<div class="row justify-content-left domain-search-page-table-header">{/* domain table prices header */}
	<div class="col-md-3">
	<span>domain</span>{/* domain title */}
	</div>
	
	<div class="col-md-2">
	<span>registration</span>{/* domain title */}
	</div>
	
	<div class="col-md-3">
	<span>transfer</span>{/* domain title */}
	</div>
	
	<div class="col-md-2">
	<span>who privacy</span>{/* domain title */}
	</div>
	
	<div class="col-md-2">
	<span>actions</span>{/* domain title */}
	</div>
	
	</div>{/* end domain table prices header */}
	
	
	
	<div class="row justify-content-left domain-search-page-table-body">{/* domain table prices body */}
	<div class="col-md-3">
	<span class="ltd-name"><small class="ltd-name-small-phone">domain</small> .net</span>{/*  domain ltd */}
	</div>
	
	<div class="col-md-2 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">registration</small>
	<span class="badge badge-pill badge-primary"> ₹1299</span>{/*  domain price */}
	</div>
	
	<div class="col-md-3 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">transfer</small>
	<span class="badge badge-pill badge-info"> ₹922</span>{/*  domain transfer */}
	</div>
	
	<div class="col-md-2 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">who privacy</small>
	<span class="badge badge-pill badge-success"> free</span>{/*  whois price */}
	</div>
	
	<div class="col-md-2">
	<a data-toggle="tooltip" data-placement="bottom" title="registration" href="#"><i class="fas fa-plus"></i></a>{/*  domain table actions */}
	<a data-toggle="tooltip" data-placement="bottom" title="transfer" href="#"><i class="fas fa-exchange-alt"></i></a>{/*  domain table actions */}
	<a data-toggle="tooltip" data-placement="bottom" title="delete" href="#"><i class="far fa-trash-alt"></i></a>{/*  domain table actions */}
	</div>
	
	</div>{/* end domain table prices body */}
	
	
	<div class="row justify-content-left domain-search-page-table-body">{/* domain table prices body */}
	<div class="col-md-3">
	<span class="ltd-name"><small class="ltd-name-small-phone">domain</small> .store</span>{/*  domain ltd */}
	</div>
	
	<div class="col-md-2 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">registration</small>
	<span class="badge badge-pill badge-primary"> ₹1299</span>{/*  domain price */}
	</div>
	
	<div class="col-md-3 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">transfer</small>
	<span class="badge badge-pill badge-info"> ₹922</span>{/*  domain transfer */}
	</div>
	
	<div class="col-md-2 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">who privacy</small>
	<span class="badge badge-pill badge-success"> free</span>{/*  whois price */}
	</div>
	
	<div class="col-md-2">
	<a data-toggle="tooltip" data-placement="bottom" title="registration" href="#"><i class="fas fa-plus"></i></a>{/*  domain table actions */}
	<a data-toggle="tooltip" data-placement="bottom" title="transfer" href="#"><i class="fas fa-exchange-alt"></i></a>{/*  domain table actions */}
	<a data-toggle="tooltip" data-placement="bottom" title="delete" href="#"><i class="far fa-trash-alt"></i></a>{/*  domain table actions */}
	</div>
	
	</div>{/* end domain table prices body */}
	
	
	<div class="row justify-content-left domain-search-page-table-body">{/* domain table prices body */}
	<div class="col-md-3">
	<span class="ltd-name"><small class="ltd-name-small-phone">domain</small> .org</span>{/*  domain ltd */}
	</div>
	
	<div class="col-md-2 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">registration</small>
	<span class="badge badge-pill badge-primary"> ₹1299</span>{/*  domain price */}
	</div>
	
	<div class="col-md-3 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">transfer</small>
	<span class="badge badge-pill badge-info"> ₹922</span>{/*  domain transfer */}
	</div>
	
	<div class="col-md-2 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">who privacy</small>
	<span class="badge badge-pill badge-success"> free</span>{/*  whois price */}
	</div>
	
	<div class="col-md-2">
	<a data-toggle="tooltip" data-placement="bottom" title="registration" href="#"><i class="fas fa-plus"></i></a>{/*  domain table actions */}
	<a data-toggle="tooltip" data-placement="bottom" title="transfer" href="#"><i class="fas fa-exchange-alt"></i></a>{/*  domain table actions */}
	<a data-toggle="tooltip" data-placement="bottom" title="delete" href="#"><i class="far fa-trash-alt"></i></a>{/*  domain table actions */}
	</div>
	
	</div>{/* end domain table prices body */}
	
	
	<div class="row justify-content-left domain-search-page-table-body">{/* domain table prices body */}
	<div class="col-md-3">
	<span class="ltd-name"><small class="ltd-name-small-phone">domain</small> .info</span>{/*  domain ltd */}
	</div>
	
	<div class="col-md-2 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">registration</small>
	<span class="badge badge-pill badge-primary"> ₹199</span>{/*  domain price */}
	</div>
	
	<div class="col-md-3 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">transfer</small>
	<span class="badge badge-pill badge-info"> ₹92</span>{/*  domain transfer */}
	</div>
	
	<div class="col-md-2 ltd-name-small-phone-styles">
	<small class="ltd-name-small-phone">who privacy</small>
	<span class="badge badge-pill badge-success"> free</span>{/*  whois price */}
	</div>
	
	<div class="col-md-2">
	<a data-toggle="tooltip" data-placement="bottom" title="registration" href="#"><i class="fas fa-plus"></i></a>{/*  domain table actions */}
	<a data-toggle="tooltip" data-placement="bottom" title="transfer" href="#"><i class="fas fa-exchange-alt"></i></a>{/*  domain table actions */}
	<a data-toggle="tooltip" data-placement="bottom" title="delete" href="#"><i class="far fa-trash-alt"></i></a>{/*  domain table actions */}
	</div>
	
	</div>{/* end domain table prices body */}
	
	
	</div>{/* end domain table prices */}
	</section>
	
	
	
	<section class="padding-100-0">
	<div class="container pad-bt-100">
	<div class="tittle-simple-one"><h5>What you get with every domain<span>Domain registration shows that the digital assets are yours and it makes your stake <br/> on that name of your dream Startup or business. </span></h5></div>{/* title */}

	<div class="row mr-tp-50">
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fab fa-amazon-pay grandient-blue-text-color"></i>
        <a href="#"> <h4>  Privacy Protection </h4> </a>
        <p>While Registering a domains name we need to fill various details like Name, Email, Contact number etc. later on which published in Whois data and it can be used for Spam purpose so we provide complete privacy protection to hide the details from published data.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-bug grandient-red-text-color"></i>
        <a href="#"> <h4> DNS Management </h4> </a>
        <p>Easily Manage your DNS Records, IP address, Subdomains, FTP accounts, and many more things.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-database grandient-green-text-color"></i>
        <a href="#"> <h4> Host Names </h4> </a>
        <p>Create Hostname of your choice with simple steps.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fab fa-expeditedssl grandient-yellow-text-color"></i>
        <a href="#"> <h4> Domain Forwarding </h4> </a>
        <p>Automatically Transfer the traffic from one Domain name to another domain name which you want just using it.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-gem grandient-blue-t-text-color"></i>
        <a href="#"> <h4>  Domain Theft protection </h4> </a>
        <p>It prevents unauthorized transfers of the domain name without the permission of the owner.</p>
    </div>
    </div>
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-gift grandient-blue-th-text-color"></i>
        <a href="#"> <h4> Easy to use control panel</h4> </a>
        <p>You can easily manage the Domain Setting, Domains transfer with our easy to use control panel.</p>
    </div>
    </div>
	
	</div>
	</div>
	
	
	<div class="container">
	<div class="row justify-content-center domain-homepage-anouncement-hero mr-bt-60">{/* row */}
                <div class="col-md-6 mt-auto mb-auto">{/* col */}
                    <h4 class="domain-homepage-anouncement-title">Why do you need a <br/>domain name?</h4>{/* title */}
                    <p class="domain-homepage-anouncement-sub-title"> In this Era of Digitalization, Every Big Startup Idea and Businesses want to create their online presence so they can maximize the reach among the whole world. A domain name provides you assets of Digital real estate which claims your stack on the domain name.</p>{/* text */}
                    <p class="domain-homepage-anouncement-sub-title-two"> It is a digital piece of your brand identity which Spread the Spark of your brand over the Internet.</p>{/* text */}
                    <br/>

                    {/*------------------------------------------- first domain anoncment order -------------------------------------------*/}

                    {/* The domain anoncment order */}
                    <div id="fisrt-domains-offre-content" class="domain-homepage-anouncement-speacial">
                        <span class="domain-tci">.tel</span>
                        {/* The domain anoncment name */}
                        <a id="fisrt-domains-offre" class="domain-tci-order"><span>register</span></a>
                        {/* The domain anoncment register button */}
                        <div class="domain-tci-buttons">
                            {/* The domain anoncment finel buttons */}
                            <i id="fisrt-domains" data-toggle="tooltip" data-placement="top" title="add to cart" class="far fa-check-circle domain-tci-check"></i>
                            <i id="fisrt" data-toggle="tooltip" data-placement="top" title="cancel order" class="far fa-times-circle domain-tci-cancel"></i>
                        </div>
                        {/* end domain anoncment finel buttons */}
                        <form class="domain-homepage-anouncement-speacial-form">
                            {/* The domain anoncment order form */}
                            <input placeholder="entre your domain name" type="text" class="form-control input-search-text-special" required=""/>
                        </form>
                        {/* end domain anoncment order form */}

                        <div class="domain-tci-added-to-card-mesage">
                            {/* The domain anoncment success message */}
                            <span>this domain added successfully to your cart <a href="#">checkout</a></span>
                        </div>
                        {/* end domain anoncment success message */}
                    </div>
                    {/* end domain anoncment order */}

                    {/*------------------------------------------- second domain anoncment order -------------------------------------------*/}

                    <div id="second-domains-offre-content" class="domain-homepage-anouncement-speacial">
                        <span class="domain-tci">.one</span>
                        {/* The domain anoncment name */}
                        <a id="second-domains-offre" class="domain-tci-order"><span>register</span></a>
                        {/* The domain anoncment register button */}
                        <div class="domain-tci-buttons">
                            {/* The domain anoncment finel buttons */}
                            <i id="second-domains" data-toggle="tooltip" data-placement="top" title="add to cart" class="far fa-check-circle domain-tci-check"></i>
                            <i id="second" data-toggle="tooltip" data-placement="top" title="cancel order" class="far fa-times-circle domain-tci-cancel"></i>
                        </div>
                        {/* end domain anoncment finel buttons */}
                        <form class="domain-homepage-anouncement-speacial-form">
                            {/* The domain anoncment order form */}
                            <input placeholder="entre your domain name" type="text" class="form-control input-search-text-special" required=""/>
                        </form>
                        {/* end domain anoncment order form */}

                        <div class="domain-tci-added-to-card-mesage">
                            {/* The domain anoncment success message */}
                            <span>this domain added successfully to your cart <a href="#">checkout</a></span>
                        </div>
                        {/* end domain anoncment success message */}
                    </div>
                    {/* end domain anoncment order */}

                    {/*------------------------------------------- third domain anoncment order -------------------------------------------*/}

                    <div id="third-domains-offre-content" class="domain-homepage-anouncement-speacial">
                        <span class="domain-tci">.hosting</span>
                        {/* The domain anoncment name */}
                        <a id="third-domains-offre" class="domain-tci-order"><span>register</span></a>
                        {/* The domain anoncment register button */}
                        <div class="domain-tci-buttons">
                            {/* The domain anoncment finel buttons */}
                            <i id="third-domains" data-toggle="tooltip" data-placement="top" title="add to cart" class="far fa-check-circle domain-tci-check"></i>
                            <i id="third" data-toggle="tooltip" data-placement="top" title="cancel order" class="far fa-times-circle domain-tci-cancel"></i>
                        </div>
                        {/* end domain anoncment finel buttons */}
                        <form class="domain-homepage-anouncement-speacial-form">
                            {/* The domain anoncment order form */}
                            <input placeholder="entre your domain name" type="text" class="form-control input-search-text-special" required=""/>
                        </form>
                        {/* end domain anoncment order form */}

                        <div class="domain-tci-added-to-card-mesage">
                            {/* The domain anoncment success message */}
                            <span>this domain added successfully to your cart <a href="#">checkout</a></span>
                        </div>
                        {/* end domain anoncment success message */}
                    </div>
                    {/* end domain anoncment order */}

                </div>{/* end col */}

                <div class="col-md-6 row justify-content-center">
                    <div class="col-md-6">{/* col */}
                        <div class="domain-homepage-anouncement-box">{/* domain features box */}
                            <div class="domain-homepage-anouncement-box-img">
                                <img src="img/svgs/book.svg" alt="" />
                            </div>
                            <h5>Claim your ownership </h5>
                            <p>Every Startup and Business has their Unique Name which drives the motion of a brand. So claim your ownership on that Domain which brings traffic to your website.</p>

                        </div>{/* end domain features box */}

                        <div class="domain-homepage-anouncement-box">{/* domain features box */}
                            <div class="domain-homepage-anouncement-box-img">
                                <img src="img/svgs/ring.svg" alt="" />
                            </div>
                            <h5>Protect Your Rights</h5>
                            <p>Every Startup and Business has their Unique Name which drives the motion of a brand. So claim your ownership on that Domain which brings traffic to your website.</p>

                        </div>{/* end domain features box */}
                    </div>{/* end col */}

                    <div class="col-md-6">{/* col */}
                        <div class="domain-homepage-anouncement-box margin-top-150">{/* domain features box */}
                            <div class="domain-homepage-anouncement-box-img">
                                <img src="img/svgs/dollar.svg" alt="" />
                            </div>
                            <h5>Setup professional Email Address </h5>
                            <p>Name@yourdomain.com is the more impressive way to connect with peoples in your network. it is a good way to create that professional communication with your personalized email.</p>

                        </div>{/* end domain features box */}

                    </div>{/* end col */}

                </div>{/* end row */}
         </div>		
	</div>
	</section>
	<Footer/>
</body>
        );
    }
}

export default Domains;