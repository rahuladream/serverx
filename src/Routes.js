import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Base from './components/Layout/Base';
import Home from './components/Home';
import AboutUs from './components/AboutUs';
import HelpCenter from './components/Help/HelpCenter'
import KnowledgeBase from './components/Help/KnowledgeBase';
import Contact from './components/Contact';
import  Domain  from './components/Domains';
import WebHosting from './components/WebHosting';
import ResellerHosting from './components/ResellerHosting';
import WordpressHosting from './components/WordpressHosting';
import CloudHosting from './components/CloudHosting';
import DDOS from './components/DDOS';
import NotFound from './components/NotFound';
import Blog from './components/Blog/Blog';


const Routes = ({ location }) => {
    const currentKey = location.pathname.split('/')[1] || '/';
    const timeout = { enter: 500, exit: 500 };
    const animationName = 'rag-fadeIn'
        return (
            // Layout component wrapper
            // Use <BaseHorizontal> to change layout
              <TransitionGroup>
                <CSSTransition key={currentKey} timeout={timeout} classNames={animationName} exit={false}>
                    <div>
                        <Switch location={location}>
                            {/*Dashboard*/}
                              <Route path="/aboutus" component={AboutUs}/>
                              <Route path="/helpcenter" component={HelpCenter}/>
                              <Route path="/knowledgebase" component={KnowledgeBase}/>
                              <Route path="/contactus" component={Contact}/>
                              <Route path="/domains" component={Domain}/>
                              <Route path="/webhosting" component={WebHosting}/>
                              <Route path="/resellerhosting" component={ResellerHosting}/>
                              <Route path="/wordpresshosting" component={WordpressHosting}/>
                              <Route path="/cloudhosting" component={CloudHosting}/>
                              <Route path="/ddos" component={DDOS}/>
                              <Route path="/notfound" component={NotFound} />
                              <Route path="/blog" component={Blog}/>
                              <Route path="/" component={Home}/>
                        </Switch>
                    </div>
                </CSSTransition>
              </TransitionGroup>
        )
}

export default withRouter(Routes);
