import React from 'react';
import Header from './Layout/Header';
import Footer from './Layout/Footer';
import Menu from './Layout/Menu';
import FunFact from './Layout/Funfact';
class AboutUs extends React.Component {
    render() {
        return(
            <body>
     <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-header">
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>
        </div>
        <div className="header-animation">
        <div id="particles-bg"></div>
            <span className="courve-gb-hdr-top"></span>
            <a className="support-header-ring" href="#"><img src="img/svgs/support.svg" alt="" /> <span>support team</span></a>
        </div>
        <Menu />
        <main className="inner cover header-heeadline-title mb-auto">
         <h5><span className="blok-on-phon">About Us</span></h5>
		 <p>We are in the business since 2018</p>
	 
        </main>
        <div className="mt-auto"></div>
    </div>

	
	
	
	<section className="padding-100-0">
	<div className="container">
	<div className="row">
    <div className="col-md-12 text-center about-us-page-title">
    <h6 className="about-us-page-title-title"> FROM OCT,2018 </h6>
    <h2 className="about-us-page-title-sub-title">We are an independent <br/>strategic advertising agency.</h2>
    <p className="about-us-page-title-text"> Server X is one of the Affordable and reliable platforms for hosting services. You are thinking of starting your own website then you are at the right place.</p>
    
	<a className="about-us-contact-way"><i className="fas fa-at"></i> support@ServerX.in</a>
	<a className="about-us-contact-way"><i className="fas fa-phone"></i> 70 6011 5192</a>
	</div>
    </div>
	
	</div>
	</section>
	
	
	<section className="padding-40-0-100">
	<div className="container">
	<div className="row">
	<div className="col-md-6 row">
	<div className="col-md-6">
    <div className="main-service-box mt-20 text-left">
        <i className="fab fa-amazon-pay grandient-blue-text-color font-size-33"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Strength is all matter for website. We can handle thousand strength of server response at every second.</p>
    </div>
    </div>
	
	<div className="col-md-6">
    <div className="main-service-box mt-20 text-left">
        <i className="fas fa-bug grandient-red-text-color font-size-33"></i>
        <a href="#"> <h4> Security </h4> </a>
        <p>We have security Analytics installed on every server so that we can easily detect the security threat.</p>
    </div>
    </div>
	

	<div className="col-md-6">
    <div className="main-service-box mt-20 text-left">
        <i className="fas fa-database grandient-green-text-color font-size-33"></i>
        <a href="#"> <h4> Storage  </h4> </a>
        <p>We providing more storage at much more low prices and just for you .</p>
    </div>
    </div>
	
	
	<div className="col-md-6">
    <div className="main-service-box mt-20 text-left">
        <i className="fab fa-expeditedssl grandient-yellow-text-color font-size-33"></i>
        <a href="#"> <h4> Uptime </h4> </a>
        <p>We care about your business, startup that is why our server uptime is 24*7 at their peak strength.</p>
    </div>
    </div>

	</div>
	
	<div className="col-md-6 text-right no-display-phone">
	<div className="image-with-outer-border">
	<img src="img/custom/dataserver.png" alt="" />
	
	</div>
	</div>
	</div>
	</div>
	</section>
	
	<section className="padding-40-0-100 gradient-bb-white">
	<div className="container">
	<h5 className="title-left-bold">why you have to chose us <span></span></h5>
	
	<div className="row mr-tp-70">
	<div className="col-md-3">
	<div className="container-features-about-us">
	<div className="features-about-us-number">
	<div className="bulb-bllue-number">1</div>
	</div>
	<div className="features-about-us-text">
	<h5>Best Web Hosting <br/>Provider </h5>
	</div>
	</div>
	</div>
	
	<div className="col-md-3">
	<div className="container-features-about-us">
	<div className="features-about-us-number">
	<div className="bulb-bllue-number">2</div>
	</div>
	<div className="features-about-us-text">
	<h5>Super Sonic Support <br/> Team</h5>
	</div>
	</div>
	</div>
	
	
	<div className="col-md-3">
	<div className="container-features-about-us">
	<div className="features-about-us-number">
	<div className="bulb-bllue-number">3</div>
	</div>
	<div className="features-about-us-text">
	<h5>Cheapest Domain <br/> Names </h5>
	</div>
	</div>
	</div>
	
	
	<div className="col-md-3">
	<div className="container-features-about-us">
	<div className="features-about-us-number">
	<div className="bulb-bllue-number">4</div>
	</div>
	<div className="features-about-us-text">
	<h5>Thousands of Happy <br/> Customers</h5>
	</div>
	</div>
	</div>
	
	
	</div>
	</div>
	</section>
    <FunFact />
	<Footer/>
</body>
        );
    }
}

export default AboutUs;