import React from 'react';
import Header from './Layout/Header';
import Footer from './Layout/Footer';
import Menu from './Layout/Menu';

class CloudHosting extends React.Component {
    render() {
        return(
            <body>
     <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-header">
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>
        </div>
        <div className="header-animation">
        <div id="particles-bg"></div>
            <span className="courve-gb-hdr-top"></span>
            <a className="support-header-ring" href="#"><img src="img/svgs/support.svg" alt="" /> <span>support team</span></a>
        </div>
        <Menu />
        
       
        <main className="inner cover header-heeadline-title mb-auto">

         <h5><span className="blok-on-phon">cloud hosting</span></h5>
		 <p>Clean Code for Startups, Companies, Designers and Developers</p>
		 
		 <div className="container">
		 <nav aria-label="breadcrumb">
		 <ol className="breadcrumb not-index-breadcrumb-header justify-content-center">
  		   <li className="breadcrumb-item"><a href="#">Homepage</a></li>
  		   <li className="breadcrumb-item"><a href="#">hosting services</a></li>
 		    <li className="breadcrumb-item active" aria-current="page">cloud hosting</li>
 		  </ol>
		 </nav>
		 </div>
		 
        </main>

        <div className="mt-auto"></div>
    </div>
   
	
	<section class="second-items-home padding-40-0-0">
	<div class="container">
	<div class="row justify-content-center">
	<div class="col-md-3">
	<div class="pricing-plan-one">
	<span class="box-overlow-cari"></span>
	<h5 class="title-plan"><span>start building your empire</span> basic </h5>
	<div class="price-palan">
	<span><i>$</i>75<b>/monthly</b></span>
	</div>
	<ul class="body-plan">
    <li><b>100 MB</b> Disk Space</li>
	<li><b>Unlimited</b> Bandwith</li>
	<li><b>standard</b> performance</li>
	<li>free <b>SSL</b> Certificate</li>
	<li>free <b>Domain</b></li>
	</ul>
	<div class="purshase-plan">
	<a href="#">purshase</a>
	</div>
	</div>
	</div>
	
	
	<div class="col-md-3">
	<div class="pricing-plan-one spceale-plan">
	<span class="best-selle-tag-plan"><b>best selle</b></span>
	<span class="box-overlow-cari"></span>
	<h5 class="title-plan"><span>start building your empire</span> basic </h5>
	<div class="price-palan">
	<span><i>$</i>75<b>/monthly</b></span>
	</div>
	<ul class="body-plan">
    <li><b>100 MB</b> Disk Space</li>
	<li><b>Unlimited</b> Bandwith</li>
	<li><b>standard</b> performance</li>
	<li>free <b>SSL</b> Certificate</li>
	<li>free <b>Domain</b></li>
	</ul>
	<div class="purshase-plan">
	<a href="#">purshase</a>
	</div>
	</div>
	</div>
	
	<div class="col-md-3">
	<div class="pricing-plan-one">
	<span class="box-overlow-cari"></span>
	<h5 class="title-plan"><span>start building your empire</span> basic </h5>
	<div class="price-palan">
	<span><i>$</i>75<b>/monthly</b></span>
	</div>
	<ul class="body-plan">
    <li><b>100 MB</b> Disk Space</li>
	<li><b>Unlimited</b> Bandwith</li>
	<li><b>standard</b> performance</li>
	<li>free <b>SSL</b> Certificate</li>
	<li>free <b>Domain</b></li>
	</ul>
	<div class="purshase-plan">
	<a href="#">purshase</a>
	</div>
	</div>
	</div>
	
	
	</div>
	</div>

	
	</section>
	
	
	
	
	<section class="our-pertners">
	<div class="container"> {/* CONTAINER */}
	<h2 class="d-none">our pertners</h2>
	<div class="owl-carousel pertners-carousel owl-theme">
    <div class="item"><a href="#"> <img src="img/pertners/logo1.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo2.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo3.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo4.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo5.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo1.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo2.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo3.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo4.png" alt="" /> </a></div>
	<div class="item"><a href="#"> <img src="img/pertners/logo5.png" alt="" /> </a></div>
    </div>
	
	</div>
	</section>

	
	
	<section class="padding-100-0 gray-background">
	<div class="container">
	<h5 class="title-left-bold text-center">why you have to chose us <span>is simply dummy text of the printing and typesetting industry.<br/> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </span></h5>
	
	<div class="row mr-tp-80">
	<div class="col-md-3">
	<div class="container-features-about-us">
	<div class="features-about-us-number">
	<div class="bulb-bllue-number">1</div>
	</div>
	<div class="features-about-us-text">
	<h5>Best Webhosting,<br/> Provider</h5>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit placerat, porttitor tristique sem luctus pharetra est sit amet nunc pulvinar ante dolor.</p>
	</div>
	</div>
	</div>
	
	<div class="col-md-3">
	<div class="container-features-about-us">
	<div class="features-about-us-number">
	<div class="bulb-bllue-number">2</div>
	</div>
	<div class="features-about-us-text">
	<h5>Cheapest Domains,<br/>  Names</h5>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit placerat, porttitor tristique sem luctus pharetra est sit amet nunc pulvinar ante dolor.</p>
	</div>
	</div>
	</div>
	
	
	<div class="col-md-3">
	<div class="container-features-about-us">
	<div class="features-about-us-number">
	<div class="bulb-bllue-number">3</div>
	</div>
	<div class="features-about-us-text">
	<h5>Fast Support,<br/> Team</h5>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit placerat, porttitor tristique sem luctus pharetra est sit amet nunc pulvinar ante dolor.</p>
	</div>
	</div>
	</div>
	
	
	<div class="col-md-3">
	<div class="container-features-about-us">
	<div class="features-about-us-number">
	<div class="bulb-bllue-number">4</div>
	</div>
	<div class="features-about-us-text">
	<h5>Thousands Of Happy,<br/> Clients</h5>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit placerat, porttitor tristique sem luctus pharetra est sit amet nunc pulvinar ante dolor.</p>
	</div>
	</div>
	</div>
	
	
	</div>
	</div>
	</section>

	
	
	<section class="server-place-section">{/* start our servers place section */}
        <div class="container zindextow">{/* start container */}
            <div class="row justify-content-center">{/* start row */}
                <div class="col-md-5 row justify-content-center text-reve-map-place">
                    <div class="col-md-6">
                        <h5>brazil server</h5>{/* title */}
                        <p><span>address</span> tchuk slov 133, contral park,brazil</p>{/* address */}
                        <p><span>phone</span>00213 123-45-67-89 {/* phones */}
                            <br/>00213 123-45-67-89</p>{/* phones */}
                        <p><span>email</span> support@coodiv.net</p>{/* email */}
                    </div>

                    <div class="col-md-6">
                        <h5>paris server</h5>{/* title */}
                        <p><span>address</span> tchuk slov 133, contral park,paris</p>{/* address */}
                        <p><span>phone</span>00213 123-45-67-89 {/* phones */}
                            <br/>00213 123-45-67-89</p>{/* phones */}
                        <p><span>email</span> support@coodiv.net</p>{/* email */}
                    </div>
                </div>

                <div class="col-md-7">{/* col */}
                    <div class="map-gene-server-place">{/* start map place */}
                        <img src="img/map/map-world.png" alt="" />{/* map */}
                        <span data-toggle="tooltip" data-placement="top" title="brazil" style={{top: 68, left: 33}} className="place"></span>
                        <span data-toggle="tooltip" data-placement="top" title="paris" style={{top: 35,left: 47}} className="place"></span>
                    </div>{/* end map place */}
                </div>{/* end col */}
				
            </div>{/* end row */}
        </div>{/* end container */}
    </section>{/* end our servers place section */}

	
	<section class="padding-90-0-100 gray-background position-relative overflow-hidden">
	<span class="section-with-moon-back-under"></span>
	<span class="section-with-moon-men-quastions-under"></span>
	<div class="container">
	<div class="tittle-simple-one"><h5>Frequently asked questions<span>is simply dummy text of the printing is simply dummy,<br/> is simply dummy text of the printing.</span></h5></div>

	<div class="row justify-content-left mr-tp-50">
	<div class="col-md-6">
	<div class="nuhost-filter-container"> {/* start q&a filter */}
	<i class="fas fa-search"></i>
	<input type="text" id="nuhost-filter-input" onkeyup="FilterListSection()" placeholder="Search for quastions here.."/> {/* q&a filter input */}
    </div>
	<div class="nuhost-filter-list-container"> {/* q&a filter list container */}
	
	<div class="filter-content-box" id="go-to-qst-01-content"> {/* start q&a first content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>how can i order new host ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a first content box */}
	
	<div class="filter-content-box" id="go-to-qst-02-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	<div class="filter-content-box" id="go-to-qst-03-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	<div class="filter-content-box" id="go-to-qst-04-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	<div class="filter-content-box" id="go-to-qst-05-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	
	<div class="filter-content-box" id="go-to-qst-06-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	
	<div class="filter-content-box" id="go-to-qst-07-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	<ul id="nuhost-filter-list"> {/* start q&a filter item list */}
    <li><a id="go-to-qst-01">how can i order new host ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-02">what is resellers hosting ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-03">i want new domain name <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-04">can i pay with paypal ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-05">are this site secure ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-06">i want WordPress site <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-07">I want to know how i can contact you <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    </ul> {/* end q&a filter item list */}
	</div> {/* end q&a filter */}
	</div>
	</div>
	</div>
	</section>
	<Footer/>
</body>
        );
    }
}

export default CloudHosting;