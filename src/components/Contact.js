import React from 'react';
import Menu from './Layout/Menu';
import Footer from './Layout/Footer';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Contact extends React.Component {
    render(){
        return(
            <body>
            <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-headerv2">
               <div className="search-header-block">
                   <form id="pitursrach-header" name="formsearch" method="get" action="#">
                       <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                       <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                       <a className="closesrch-her-block np-dsp-block">
                           <span className="first-stright"></span>
                           <span className="second-stright"></span>
                       </a>
                   </form>
               </div>
               <div className="header-animation">
                   <div id="particles-bg"></div>
               </div>
               <Menu />
               <div className="mt-auto"></div>
           </div>
        
    <section class="padding-100-0">
	<div class="container">
	<div class="row">
    <div class="col-md-12 text-center about-us-page-title contact-us-background">
    <h6 class="about-us-page-title-title"> You still got questions ? </h6>
    <h2 class="about-us-page-title-sub-title">don't worry, you can talk <br/>to our humans.</h2>
    <a class="about-us-contact-way"><i class="fas fa-at"></i> suuport@serverx.in</a>
	<a class="about-us-contact-way"><i class="fas fa-phone"></i> 70 6011 5192 s</a>

	</div>
    </div>

	<div class="row question-area-page justify-content-left mr-tp-120">
    <div class="col-md-8">
	<div class="question-area-answer-banner">
	 <form class="row justify-content-center form-contain-home contact-page-form-send" id="ajax-contact" method="post" action="mailer.php">{/* start form */}
	 <h5>get in touch <span>discouver how can our services grow your business.</span></h5>
	 <div id="form-messages"></div>{/* form message */}
					
                <div class="col-md-6">{/* start col */}
                        <div class="field input-field">
                            <input class="form-contain-home-input" type="text" id="name" name="name" required/>{/* input */}
                            <span class="input-group-prepend">entre your name</span>{/* label */}
                        </div>
                </div>{/* end col */}
					
                <div class="col-md-6">{/* start col */}
                        <div class="field input-field">
                            <input class="form-contain-home-input" type="email" id="email" name="email" required/>{/* input */}
                            <span class="input-group-prepend">entre your email</span>{/* label */}
                        </div>
                </div>{/* end col */}
					
                <div class="col-md-12">{/* start col */}
                    <div class="field input-field">
                    <textarea class="form-contain-home-input" id="message" name="message" required></textarea>{/* textarea */}
                     <span class="input-group-prepend">entre your message</span>{/* label */}
                    </div>
                </div>{/* end col */}
					
                <div class="btn-holder-contect">
                <button type="submit">Send</button>{/* submit button */}
			    </div>

     </form>{/* end form */}
	</div>
	</div>
	
	<div class="col-md-4">
	<div class="contact-other-method-box">
	<h5>need more help ? <span>you are already a customer?, than open new ticket </span></h5>
	<a class="btn-order-default-nuhost" href="knowledgebase.html">New Ticket</a>
	</div>
	
	<div class="contact-other-method-box">
	<h5>knowledgebase</h5>
	<Link to = '/knowledgebase' class="btn-order-default-nuhost">go to knowledgebase</Link>
	</div>
	</div>

	</div>
	
	
	</div>
	</section>
        <Footer/>
       </body>  
        )
    }
}
export default Contact;