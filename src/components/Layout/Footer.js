import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
const Footer = props => (
    <section className="footer-coodiv-thm pding-50">
    <div className="container">
        <div className="row justify-content-center">
            <div className="col-md-4">
                <a className="footer-brand" href="#">
                    <img src="img/header/logo-white.png" alt=""/>
                </a>

                <a className="footer-contact-a-hm"><i className="fas fa-life-ring"></i> 70 6011 5192</a>
                <a className="footer-contact-a-hm"><i className="fas fa-microphone"></i> support@serverx.in</a>
                <a className="footer-contact-a-hm"><i className="fas fa-map-marker-alt"></i> 48 Bajrang nagar sikandara agra 282007</a>
            </div>

            <div className="col-md-2 quiq-links-footer-mb-st">
                <h5 className="footer-title-simple">Quick Links</h5>
                <ul className="main-menu-footer-mn">
                    <li><a href="index.html">homepage</a></li>
                    <li><Link to="/aboutus">About us</Link></li>
                    
                    <li> <Link to="/resellerhosting">Reseller Hosting</Link></li>
                    <li><Link to="/webhosting">Web Hosting</Link></li>
                </ul>
            </div>

            <div className="col-md-2 quiq-links-footer-mb-st">
                <h5 className="footer-title-simple">About Us</h5>
                <ul className="main-menu-footer-mn">
                    <li><Link to="/knowledgebase">knowledgebase</Link></li>
                    {/* <li><Link to="/helpcenter">help-center</Link></li> */}
                    <li><Link to="/blog">blog</Link></li>
                    <li><Link to="/domains">Domains</Link></li>
                    <li><Link to="/contactus">contact</Link></li>
                </ul>
            </div>


            <div className="col-md-3 stay-in-tch-footer-mb-st">
                <h5 className="footer-title-simple">Stay In Touch</h5>
                <form className="form-contain-home-subscribe" action={this.handleForm}>
                    <input type="email" id="email-subscribe" name="email-subscribe" placeholder="entre your email please" required />
                    <button onClick={this.handleSubscriber}><i className="fas fa-paper-plane"></i></button>
                </form>

                <div className="footer-social-links">
                    <a className="facebookcc" href="#"><i className="fab fa-facebook-f"></i></a>
                    <a className="twittercc" href="#"><i className="fab fa-twitter"></i></a>
                    <a className="googlecc" href="#"><i className="fab fa-google-plus-g"></i></a>
                    <a className="dribbblecc" href="#"><i className="fab fa-dribbble"></i></a>
                </div>
                <p className="copyright-footer-p">© 2018 CodeinMood. Made with Love in India.</p>

            </div>
        </div>
    </div>
</section>
)

export default Footer;
