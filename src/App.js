import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
// import $ from 'jquery';
import Routes from './Routes';
class App extends Component {
  render() {
    const basename = '/';
    return (
      <BrowserRouter basename={basename}>
      <Routes />
      </BrowserRouter>
    );
  }
}

export default App;