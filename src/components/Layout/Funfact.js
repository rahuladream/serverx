import React from 'react';

class FunFact extends React.Component {
    render() {
        return(
            <div>
            <section className="counter-section">{/* start counter section */}
            <div className="container">
                <div className="counter-middle-icon">
                    <span className="icon-counter-sid"><i className="flaticon-063-molecules-1"></i></span>{/* icon */}
                    <h2 className="icon-counter-sid-title">our company <b>funny facts</b></h2>{/* title */}
                </div>
                <div className="row justify-content-center">{/* row */}
                    <div className="col-md-4 counter-number-tibo"><i className="flaticon-database-8"></i> <span><small className="counter">99.99</small>%</span> <b>Up time</b></div>{/* counter */}
                    <div className="col-md-4 counter-number-tibo"><i className="flaticon-plug"></i><span> <small className="counter">70</small></span><b>new website this week</b></div>{/* counter */}
                    <div className="col-md-4 counter-number-tibo"><i className="flaticon-shield-5"></i><span> <small className="counter">05</small></span> <b>Security issues</b></div>{/* counter */}
                </div>{/* end row */}
    
            </div>{/* end container */}
        </section>{/* end counter section */}
    
        <section className="server-place-section">{/* start our servers place section */}
            <div className="container zindextow">{/* start container */}
                <div className="row justify-content-center">{/* start row */}
                    <div className="col-md-5 row justify-content-center text-reve-map-place">
                        <div className="col-md-6">
                            <h5>United States</h5>{/* title */}
                            <p><span>address</span> Kansas City, Missouri</p>{/* address */}
                            <p><span>phone</span>70 6011 5192 {/* phones */}
                                <br/></p>{/* phones */}
                            <p><span>email</span> support@serverx.in</p>{/* email */}
                        </div>
                    </div>
    
                    <div className="col-md-7">{/* col */}
                        <div className="map-gene-server-place">{/* start map place */}
                            <img src="img/map/map-world.png" alt="" />{/* map */}
                          </div>{/* end map place */}
                    </div>{/* end col */}
    
                </div>{/* end row */}
            </div>{/* end container */}
        </section>
        </div>
        )
    }
}
export default FunFact;