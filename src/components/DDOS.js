import React from 'react';
import Header from './Layout/Header';
import Footer from './Layout/Footer';
import Menu from './Layout/Menu';

class DDOS extends React.Component {
    render() {
        return(
            <body>
     <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-header">
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>
        </div>
        <div className="header-animation">
		<div id="particles-bg"></div>
        </div>
        <Menu />
        
       
        <main className="inner cover header-heeadline-title mb-auto text-left">
         <div className="container position-relative">
		 <span className="img-bg-breadcrumb-header"></span>
         <h5><span className="blok-on-phon">DDOS PROTECTION </span></h5>
		 <p>Clean Code for Startups, Companies, Designers and Developers</p>
		 <nav aria-label="breadcrumb">
		 <ol className="breadcrumb not-index-breadcrumb-header justify-content-left ">
  		   <li className="breadcrumb-item"><a href="#">Homepage</a></li>
  		   <li className="breadcrumb-item"><a href="#">services</a></li>
 		    <li className="breadcrumb-item active" aria-current="page">ddos protection</li>
 		  </ol>
		 </nav>
		 </div>
		 
        </main>

        <div className="mt-auto"></div>
    </div>
    <section class="our-pertners gray-background mr-tp-0">{/* starts our pertners section */}
        <div class="container">{/* CONTAINER */}
		<h2 class="d-none">our pertners</h2>
            <div class="owl-carousel pertners-carousel owl-theme">{/* start owl carousel */}
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo1.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo2.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo3.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo4.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo5.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo1.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo2.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo3.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo4.png" alt="" /> </a>
				</div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo5.png" alt="" /> </a>
                </div>{/* end item */}
				
            </div>{/* end owl carousel */}

        </div>{/* end CONTAINER */}
    </section>{/* end our pertners section */}

	
	
	
	<section class="padding-100-0-0">
	<div class="container">{/* CONTAINER */}
	<div class="row">
    <div class="col-md-12 text-center about-us-page-title">
    <h6 class="about-us-page-title-title"> what is a DDOS attack? </h6>
    <h2 class="about-us-page-title-sub-title">Malicious attempts to make a server <br/>unvailable to users !</h2>
    <p class="about-us-page-title-text"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
	</div>
    </div>
	
	<div class="row mr-tp-50">
	<div class="col-md-12 text-center position-relative">
	<div class="screen-message-error-absolute">
	<img src="img/svgs/warning.svg" alt=""/>
	<h5>website overload</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
	</div>
	<img class="broswer-512" src="img/backrounds/broswer.png" alt="" />
	</div>
	</div>

	</div>
	</section>


	
	<section class="padding-100-0 gray-background mr-tp--120">
	<div class="container">{/* CONTAINER */}
	
	<div class="row mr-tp-50">
    <div class="col-md-12 text-center about-us-page-title">
    <h2 class="about-us-page-title-sub-title mr-bt-5">advanced DDos protection</h2>
	<p class="about-us-page-title-text"> automatic & affordable !</p>
	</div>
    </div>
	
	<div class="row justify-content-center mr-tp-50">
	<div class="col-md-4">
	<div class="ddos-attaks-plans">
	<div class="ddos-attaks-plans-header">
	<span class="first-color">$15</span><span>+$0/mo</span>
	</div>
	
	<div class="ddos-attaks-plans-body">
	<ul>
	<li>Free with all hosting plans</li>
	<li>Up to 15GBps attack trafic</li>
	<li>Layer of 3/4 attaks</li>
	<li>DNS amplification</li>
	<li>Layer 7 attaks</li>
	<li>Ack attaks</li>
	</ul>
	</div>
	<a class="ddos-attaks-plans-footer grandient-green-text-color" href="#">add to cart</a>
	</div>
	</div>
	
	<div class="col-md-4">
	<div class="ddos-attaks-plans">
	<div class="ddos-attaks-plans-header">
	<span class="second-color">$25</span><span>+$5/mo</span>
	</div>
	
	<div class="ddos-attaks-plans-body">
	<ul>
	<li>Free with all hosting plans</li>
	<li>Up to 15GBps attack trafic</li>
	<li>Layer of 3/4 attaks</li>
	<li>DNS amplification</li>
	<li>Layer 7 attaks</li>
	<li>Ack attaks</li>
	</ul>
	</div>
	<a class="ddos-attaks-plans-footer grandient-red-text-color" href="#">add to cart</a>
	</div>
	</div>
	
	<div class="col-md-4">
	<div class="ddos-attaks-plans">
	<div class="ddos-attaks-plans-header">
	<span class="third-color">$30</span><span>+$7/mo</span>
	</div>
	
	<div class="ddos-attaks-plans-body">
	<ul>
	<li>Free with all hosting plans</li>
	<li>Up to 15GBps attack trafic</li>
	<li>Layer of 3/4 attaks</li>
	<li>DNS amplification</li>
	<li>Layer 7 attaks</li>
	<li>Ack attaks</li>
	</ul>
	</div>
	<a class="ddos-attaks-plans-footer grandient-blue-text-color" href="#">add to cart</a>
	</div>
	</div>
	
	
	</div>
	
	
	</div>
	</section>
	
	
	
	<div class="padding-100-0">
	<div class="container">
	<div class="tittle-simple-one"><h5>What you get with every plan<span>is simply dummy text of the printing.<br/> typesetting industry.</span></h5></div>{/* title */}

	<div class="row mr-tp-50">
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fab fa-amazon-pay grandient-blue-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-bug grandient-red-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-database grandient-green-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fab fa-expeditedssl grandient-yellow-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-gem grandient-blue-t-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-gift grandient-blue-th-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	</div>
	</div>
	</div>
	

	
	<section class="padding-90-0-100 gray-background position-relative overflow-hidden">
	<span class="section-with-moon-back-under"></span>
	<span class="section-with-moon-men-quastions-under"></span>
	<div class="container">
	<div class="tittle-simple-one"><h5>Frequently asked questions<span>is simply dummy text of the printing is simply dummy,<br/> is simply dummy text of the printing.</span></h5></div>

	<div class="row justify-content-left mr-tp-90">
	<div class="col-md-6">
	<div class="nuhost-filter-container"> {/* start q&a filter */}
	<i class="fas fa-search"></i>
	<input type="text" id="nuhost-filter-input" onkeyup="FilterListSection()" placeholder="Search for quastions here.."/> {/* q&a filter input */}
    </div>
	<div class="nuhost-filter-list-container"> {/* q&a filter list container */}
	
	<div class="filter-content-box" id="go-to-qst-01-content"> {/* start q&a first content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>how can i order new host ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a first content box */}
	
	<div class="filter-content-box" id="go-to-qst-02-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	<div class="filter-content-box" id="go-to-qst-03-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	<div class="filter-content-box" id="go-to-qst-04-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	<div class="filter-content-box" id="go-to-qst-05-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	
	<div class="filter-content-box" id="go-to-qst-06-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	
	<div class="filter-content-box" id="go-to-qst-07-content"> {/* start q&a second content box */}
	<span class="filter-content-close" data-toggle="tooltip" data-placement="bottom" data-original-title="close">	
    <span class="first-stright"></span>
    <span class="second-stright"></span>
    </span>
	<h5>what is resellers hosting ?</h5>
	<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	<p>it has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
	</div> {/* end q&a second content box */}
	
	<ul id="nuhost-filter-list"> {/* start q&a filter item list */}
    <li><a id="go-to-qst-01">how can i order new host ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-02">what is resellers hosting ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-03">i want new domain name <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-04">can i pay with paypal ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-05">are this site secure ? <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-06">i want WordPress site <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    <li><a id="go-to-qst-07">I want to know how i can contact you <i class="fas fa-angle-right"></i></a></li> {/* filter item */}
    </ul> {/* end q&a filter item list */}
	</div> {/* end q&a filter */}
	</div>
	</div>
	
	</div>
	</section>
	
	<Footer/>
</body>
        );
    }
}

export default DDOS;