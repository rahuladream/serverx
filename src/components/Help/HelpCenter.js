import React from 'react';
import Header from '../Layout/Header';
import Footer from '../Layout/Footer';
import Menu from '../Layout/Menu';
import { APP_CONST } from '../Layout/Constants';
import Particles from 'react-particles-js';

class HelpCenter extends React.Component {
  render() {
    return(
    <body>
     <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-headerv2">
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>
        </div>
        <div className="header-animation">
            <div id="particles-bg"></div>
        </div>
        <Menu />
        <div className="mt-auto"></div>
    </div>

    
	<section class="padding-60-0-100">
    <div class="container">
	<div class="row">
	<div class="col-md-12 help-center-header ">
	<h5 class="help-center-title"><span>help center</span>wellcome to nuhost customers help center</h5>
	<p class="help-center-text"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
	</div>
	</div>
	
	<div class="tab-help-categorie">
	<div class="row justify-content-center mr-tp-80">
	<nav>
    <div class="nav our-help-center-tabs-nav" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-first-help-categorie-tab" data-toggle="tab" href="#nav-first-help-categorie" role="tab" aria-controls="nav-first-help-categorie" aria-selected="true">
	<img src="img/icons/help-01.png" alt="#" />
	<h5>my cart</h5>
	<p>is simply dummy text of the printing and typesetting industry.</p>
	</a>
    <a class="nav-item nav-link" id="nav-second-help-categorie-tab" data-toggle="tab" href="#nav-second-help-categorie" role="tab" aria-controls="nav-second-help-categorie-tab" aria-selected="false">
	<img src="img/icons/help-02.png" alt="#" />
	<h5>my invoice</h5>
	<p>is simply dummy text of the printing and typesetting industry.</p>
	</a>
    <a class="nav-item nav-link" id="nav-third-help-categorie-tab-tab" data-toggle="tab" href="#nav-third-help-categorie" role="tab" aria-selected="false">
	<img src="img/icons/help-03.png" alt="#" />
	<h5>our support team</h5>
	<p>is simply dummy text of the printing and typesetting industry.</p>
	</a>
    </div>
    </nav>
   


	</div>
	
	
	<div  class="row justify-content-left mr-tp-80">
	<div  id="nav-tabContent" class="col-md-9 tab-content">
	
	<div class="tab-pane fade show active" id="nav-first-help-categorie" role="tabpanel" aria-labelledby="nav-first-help-categorie-tab">
	<span class="help-center-box-popular">most popular categories</span>
	
	<div class="help-center-box-item">{/* start the help center box  */}
	<div class="help-center-box-icon">{/* start the help center icon  */}
	<i class="fas fa-server"></i>{/* help center item icon  */}
	</div>{/* end the help center icon  */}
	
	<div class="help-center-box-text">
	<span>our servers status </span>{/* help center item title  */}
	<span>is simply dummy text of the printing and typesetting industry.</span>{/* help center item sub title  */}
	</div>
	<a class="help-center-box-link" href="Privacy.html"><i class="fas fa-angle-right"></i></a>{/* help center item link  */}
	</div>{/* end the help center box  */}
	

	<div class="help-center-box-item">{/* start the help center box  */}
	<div class="help-center-box-icon">{/* start the help center icon  */}
	<i class="fas fa-binoculars"></i>{/* help center item icon  */}
	</div>{/* end the help center icon  */}
	
	<div class="help-center-box-text">
	<span>our domains services </span>{/* help center item title  */}
	<span>is simply dummy text of the printing and typesetting industry.</span>{/* help center item sub title  */}
	</div>
	<a class="help-center-box-link" href="Privacy.html"><i class="fas fa-angle-right"></i></a>{/* help center item link  */}
	</div>{/* end the help center box  */}
	
	
	<div class="help-center-box-item">{/* start the help center box  */}
	<div class="help-center-box-icon">{/* start the help center icon  */}
	<i class="fas fa-chart-pie"></i>{/* help center item icon  */}
	</div>{/* end the help center icon  */}
	
	<div class="help-center-box-text">
	<span>payment issue </span>{/* help center item title  */}
	<span>is simply dummy text of the printing and typesetting industry.</span>{/* help center item sub title  */}
	</div>
	<a class="help-center-box-link" href="Privacy.html"><i class="fas fa-angle-right"></i></a>{/* help center item link  */}
	</div>{/* end the help center box  */}
	
	
	
	<div class="help-center-box-item">{/* start the help center box  */}
	<div class="help-center-box-icon">{/* start the help center icon  */}
	<i class="fab fa-cloudscale"></i>{/* help center item icon  */}
	</div>{/* end the help center icon  */}
	
	<div class="help-center-box-text">
	<span>DDos attack </span>{/* help center item title  */}
	<span>is simply dummy text of the printing and typesetting industry.</span>{/* help center item sub title  */}
	</div>
	<a class="help-center-box-link" href="#"><i class="fas fa-angle-right"></i></a>{/* help center item link  */}
	</div>
	</div>
	
	
    <div class="tab-pane fade" id="nav-second-help-categorie" role="tabpanel" aria-labelledby="nav-second-help-categorie-tab">
	<h5 class="no-cat-error">there is no categories here <span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></h5>
	</div>
    <div class="tab-pane fade" id="nav-third-help-categorie" role="tabpanel">
	<h5 class="no-cat-error">there is no categories here <span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></h5>
	</div>
  
  
	
	</div>
	
	
	<div class="col-md-3">
	<h5 class="immediate-help-center-title">Need some immediate <br/>help ?</h5>
	<p class="immediate-help-center-text">our suuport team here for you 24/7</p>
	
	<a class="immediate-help-center-link" href="#">{APP_CONST.phone}</a>
	<a class="immediate-help-center-link" href="#">{APP_CONST.email}</a>
	<a class="immediate-help-center-link" href="contact.html">send a leter</a>
	</div>
	</div>
	</div>
	</div>
	
	</section>
	

	<Footer/>
</body>
        );
    }
}

export default HelpCenter;