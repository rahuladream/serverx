import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
class Menu extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0)
      }
render(){
return(
<div className="top-header-nav-home">
<div className="container">
    <nav className="navbar navbar-expand-md navbar-light header-nav-algo-coodiv header-nav-algo-coodiv-v2">
            {/* start logo place */}
            <a className="navbar-brand" href="/">
            <img className="black-bg-logo" src="img/header/logo-white.png" alt="" />
            {/* black background logo */}
            <img className="white-bg-logo" src="img/header/logo-black.png" alt="" />
            {/* white background logo */}
            <span>India</span>
            </a>
            {/* end logo place */}
        <button className="navbar-toggle offcanvas-toggle menu-btn-span-bar ml-auto" data-toggle="offcanvas" data-target="#offcanvas-menu-home">
            <span></span>
            <span></span>
            <span></span>
        </button>
        {/* start collapse navbar */}
        <div className="collapse navbar-collapse navbar-offcanvas" id="offcanvas-menu-home">
        {/* start navbar */}
            <ul className="navbar-nav ml-auto">
                 <li className="nav-item dropdown">
                    <a className="nav-link" href="/">Home</a>

                </li>

                <li className="nav-item">
                    <Link className="nav-link" to="/aboutus">About us</Link>
                </li>


                <li className="nav-item mega-dropdown">
                    <Link className="nav-link dropdown" to="#" id="megadrop-services" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Our Services <span className="caret"></span></Link>
                    <div className="dropdown-menu mega-dropdown-menu" aria-labelledby="megadrop-services">
                    <div className="our-services-mega-menu-header">
                    <div className="row justify-content-center our-services-mega-menu-header-items">
                    <Link className="col-md-2" to="/domains">
                    <i className="flaticon-063-atomic"></i>
                    <span>Domains</span>
                    </Link>

                    <Link className="col-md-2" to="/webhosting">
                    <i className="flaticon-063-compact-disc"></i>
                    <span>Web hosting</span>
                    </Link>

                    <Link className="col-md-2" to="/resellerhosting">
                    <i className="flaticon-063-dna"></i>
                    <span>Reseller</span>
                    </Link>

                    <Link className="col-md-2" to="/wordpresshosting">
                    <i className="flaticon-063-molecules-1"></i>
                    <span>WordPress</span>
                    </Link>

                    {/* <Link className="col-md-2" to="/ddos">
                    <i className="flaticon-063-saturn"></i>
                    <span>DDos</span>
                    </Link>

                    <Link className="col-md-2" to="/cloudhosting">
                    <i className="flaticon-063-nuclear"></i>
                    <span>clouds</span>
                    </Link> */}


                    </div>
                    </div>
                    </div>
                </li>


                <li className="nav-item mega-dropdown">
                    <Link className="nav-link dropdown" to="#" id="megadrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Web Hosting <span className="caret"></span></Link>
                    <div className="dropdown-menu mega-dropdown-menu" aria-labelledby="megadrop">
                        <div className="price-mega-menu-home">
                            <div className="row justify-content-center">
                                <div className="col-md-4 banner-hosting-menu">
                                    <h5>start you<br/> mini project now</h5>
                                    <p>with our mini hosting palns you will have the power to start your project</p>
                                    <Link to="#">learn more</Link>
                                    <span>from ₹ 99</span>
                                </div>
                                <div className="col-md-8 plans-hosting-menu">
                                    <h5>our hosting plans</h5>
                                    <div className="row justify-content-left">
                                        <div className="col-md-4 megamenu-plans">
                                            <h4 className="price-title">starter</h4>
                                            <span className="price-price">₹ 99 <i>/monthly</i></span>
                                            <p className="price-text">with our mini hosting palns you will have the power to start your project</p>
                                            <ul className="megamenu-plans-li">
                                                <li><i className="fas fa-check"></i> 5 GB <b>Disk space</b></li>
                                                <li><i className="fas fa-check"></i> umetered <b>bandwith</b></li>
                                                <li><i className="fas fa-check"></i> 5 MYSQL<b>database</b></li>
                                                <li><i className="fas fa-times"></i> Single <b>domain</b></li>
                                            </ul>
                                            <Link className="megamenu-plans-order" to="#">order now</Link>
                                        </div>

                                        <div className="col-md-4 megamenu-plans">
                                            <h4 className="price-title">standard</h4>
                                            <span className="price-price">₹ 149<i>/monthly</i></span>
                                            <p className="price-text">with our mini hosting palns you will have the power to start your project</p>
                                            <ul className="megamenu-plans-li">
                                                <li><i className="fas fa-check"></i> unlimated <b>disk space</b></li>
                                                <li><i className="fas fa-check"></i> unlimated <b>database</b></li>
                                                <li><i className="fas fa-check"></i> umetered <b>bandwith</b></li>
                                                <li><i className="fas fa-check"></i> Free SSL<b>certificate</b></li>
                                                <li><i className="fas fa-times"></i> free <b>domain</b></li>
                                            </ul>
                                            <Link className="megamenu-plans-order" to="#">order now</Link>
                                        </div>

                                        <div className="col-md-4 megamenu-plans no-border">
                                            <h4 className="price-title">wordpress</h4>
                                            <span className="price-price">₹ 49 <i>/monthly</i></span>
                                            <p className="price-text">with our mini hosting palns you will have the power to start your project</p>
                                            <ul className="megamenu-plans-li">
                                                <li><i className="fas fa-check"></i> 3 MYSQL <b>Database</b></li>
                                                <li><i className="fas fa-check"></i> 100 GB <b>Bandwith</b></li>
                                                <li><i className="fas fa-check"></i> 1 GB <b>disk space</b></li>
                                                <li><i className="fas fa-check"></i> Single <b>domain</b></li>
                                            </ul>
                                            <Link className="megamenu-plans-order" to="#">order now</Link>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </li>

                 {/* <li className="nav-item dropdown">
                    <Link className="nav-link dropdown" to="#" id="helpdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Help <span className="caret"></span></Link>
                    <div className="dropdown-menu" aria-labelledby="helpdropdown">
                        <Link className="dropdown-item" to="/helpcenter">help center</Link>
                        <Link className="dropdown-item" to="/knowledgebase">knowledgebase</Link>
                    </div>
                </li> */}
                <li className="nav-item">
                    <Link className="nav-link" to="/contactus">Contact us</Link>
                </li>
                {/* <li className="nav-item">
                    <Link className="nav-link" to="/blog"> Blog</Link>
                </li> */}
                <li className="nav-item">
                    <Link data-toggle="tooltip" data-placement="bottom" title="search" className="search-headr" to="#"><img src="img/svgs/search.svg" alt="" /></Link>
                </li>
            </ul>
            {/* end navbar */}

        </div>
        {/* end collapse navbar */}

        {/* start header account  */}
        <ul className="account-place-header-nav">
            <li className="nav-item dropdown">
                <a id="customarea" className="accouting-h dropdown-toggle" href="https://store.serverx.in/clientarea.php"><img src="img/svgs/avatar.svg" alt="" /></a>
                {/* <div className="dropdown-menu login-drop-down-header" aria-labelledby="customarea">

                    <form action="#" data-form="validate">

                        <div className="form-group">
                            <input type="email" name="username" placeholder="Your Email" className="form-control" required/>
                        </div>

                        <div className="form-group">
                            <input type="password" name="password" placeholder="Password" className="form-control" required/>
                        </div>

                        <p className="help-block"><Link to="#">Forgot Your Password?</Link> <Link to="#">Sign up</Link></p>
                        <button type="submit" className="btn btn-block btn-default text-uppercase">Login</button>

                    </form>

                </div> */}
            </li>
        </ul>
        {/* end header account  */}

    </nav>
</div>{/* end container */}
</div>)
}
}
export default Menu;
