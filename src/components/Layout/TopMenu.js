import React from 'react';

const TopMenu = props => (
    <div className="header-top-menu">
    <div className="container">
        <ul>
            <li><a><i className="fas fa-at"></i> support@serverx.in</a></li>
            <li><a><i className="fas fa-phone"></i> 70 6011 5192</a></li>
        </ul>

        <div className="ml-auto fl-right phone-no-display">
            <ul>
                <li><a href="#"><i className="far fa-comments"></i> support chat</a></li>
                {/* <li><a href="#"><i className="fas fa-server"></i> servers</a></li>
                <li><a href="#"><i className="fas fa-gavel"></i> Terms and Policies</a></li> */}
            </ul>
        </div>

    </div>
</div>
)
export default TopMenu;