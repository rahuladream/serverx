import React from 'react';

class MainFeature extends React.Component{
    render(){
        return(
            <section class="padding-80-0-30 white-gray-border-top">
            <div class="container">
            <div class="tittle-simple-one"><h5>Things done by Just a few clicks <span>Get Linux shared hosting with unlimited resources, Disk space, and bandwidth. At ServerX you get faster, secure, reliable, trustful and safe access to shared hosting services in India. Sophisticated yet easy to use the hosting panel. 
        ServerX always provides free SSL Certificate to give your website Green lock protection.</span></h5></div>
            
            
            <ul class="nav justify-content-center mr-tp-70 resslers-features-tabs" id="myTab" role="tablist">
            <li class="nav-item">
            <a class="nav-link active" id="Applications-tab" data-toggle="tab" href="#Applications" role="tab" aria-controls="Applications" aria-selected="true">Main Hosting Features</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" id="Features-tab" data-toggle="tab" href="#Features" role="tab" aria-controls="Features" aria-selected="false">Applications</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" id="Control-tab" data-toggle="tab" href="#Control" role="tab" aria-controls="Control" aria-selected="false">Our Hosting Control Panel</a>
            </li>
            {/* <li class="nav-item">
            <a class="nav-link" id="Support-tab" data-toggle="tab" href="#Support" role="tab" aria-controls="Support" aria-selected="false">Hosting Support</a>
            </li> */}
        
            
            </ul>
            
            
           <div class="tab-content" id="myTabContent">
             <div class="tab-pane fade show active" id="Applications" role="tabpanel" aria-labelledby="Applications-tab">
             <div class="row justify-content-center text-left">
             <div class="col-md-10 resslers-tabs-content-with-image">
             <img src="img/custom/hostingphone.jpg" alt="" />
             <div class="resslers-tabs-content-with-image-text">
             <h5>Hosting main features</h5>
             <ul>
             <li>Cpanel powered User Panel</li>
             <li>One click installation (Softaculous)</li>
             <li> One-click Backup and Restore  </li>
             <li>Unlimited Bandwidth</li>
             <li> Unlimited Disk space</li>
             <li>   Unlimited Business email accounts </li>
             <li> Unlimited Subdomains </li>
             <li>   Unlimited MySQL Data Base </li>
             <li>    Powered by Cloud Linux, cPanel, Apache, MySQL, PHP, Ruby & more </li>
             <li>    PHP 5.2 to PHP 5.4, Ruby on Rails, Perl, Python, SSH, and MySQL Databases. </li>
             <li>   Get DNS Management access  </li>
             </ul>
             </div>
             </div>
             </div>
             </div>
             <div class="tab-pane fade" id="Features" role="tabpanel" aria-labelledby="Features-tab">
             <div class="row justify-content-center text-left">
             <div class="col-md-10 resslers-tabs-content-with-image">
             <div class="resslers-tabs-content-with-image-text">
             <h5>Get access to hosting application with our Linux hosting </h5>
             <span> ServerX Provide an access of 200+ hosting application with Linux hosting to get one click installation application which helps you to develop in easy and funny way, which you will love.</span>
             <ul>
             <li>Cpanel</li>
             <li>Cloud Linux</li>
             <li>Wordpress </li>
             <li>Magento</li>
             <li>Blogger</li>
             <li>WHMCS</li>
             <li>Joomla , Wordpress , Drupal , MySQL</li>
             <li>Python Yoast etc. </li>
             </ul>
             </div>
             <img src="img/custom/hostingphone.jpg" alt="" />
             </div>
             </div>
             
             </div>
             <div class="tab-pane fade" id="Control" role="tabpanel" aria-labelledby="Control-tab">
             
             <div class="row justify-content-center">
             <div class="col-md-10 resslers-tabs-content-with-image d-block">
             <div class="resslers-tabs-content-with-image-text text-center">
             <h5>Control Panel<br/> for free <span>ServerX provides various hosting control panels with Linux hosting, that helps in the management of hosting in the easiest way  </span></h5>
            <ul>
            <li> Cpanel  </li>
             <li>  Web server (e.g. Apache, Nginx, IIS)</li>
             <li>  DNS Server</li>
             <li>  Mail server and spam filter</li>
             <li> Database , File manager ,  System monitor</li> 
            </ul>
             </div>
             </div>
             </div>
             
             </div>
             </div>
            
            
            </div>
            </section>
        )
    }
}
export default MainFeature;