import React from 'react';
import Footer from './Footer';
import Menu from './Menu';
const Header = props => (
    <body>
       {/* start header */}
       <div id="header">
        {/* start search block */}
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>

        </div>
        {/* end search block */}

        {/* start header animations */}
        <div className="header-animation">
            <div id="particles-bg"></div>
            <span className="moon-bg-her"></span>
            {/*----- header support ring -----*/}
            <a className="support-header-ring" href="#"><img src="img/svgs/support.svg" alt="" /> <span>support team</span></a>
            {/*----- end header support ring -----*/}
        </div>
        {/* end header animations */}

		{/* start top header area */}
        <div className="header-top-menu">
            <div className="container">
                <ul>
                    <li><a><i className="fas fa-at"></i> support@serverx.in</a>/a></li>
                    <li><a><i className="fas fa-phone"></i> 70 6011 5192</a></li>
                </ul>

                <div className="ml-auto fl-right phone-no-display">
                    <ul>
                        <li><a href="#"><i className="far fa-comments"></i> support chat</a></li>
                        {/* <li><a href="#"><i className="fas fa-server"></i> servers</a></li>
                        <li><a href="#"><i className="fas fa-gavel"></i> Terms and Policies</a></li> */}
                    </ul>
                </div>

            </div>
        </div>
		{/* end top header area */}

        <Menu />

        <div className="header-heeadline">{/* start main header domain search */}
            <div className="outliner-middl-ha">
                <h5><span className="blok-on-phon">we give the word best</span> <span id="typed"></span></h5>{/* main header domain search title */}
                <p>We make sure your website is fast, secure & always up</p>{/* main header domain search text */}

                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <form action="domain-results.html" id="domain-search" className="wow fadeIn" data-wow-delay="0.5s">{/* main header domain search form */}
                            <span className="space-men"></span>
                            <input id="domain-text" type="text" name="domain" placeholder="Write your domain name here.." />{/* search input */}
                            <span className="inline-button">
                            <button id="search-btn" type="submit" name="submit" value="Search"> <img src="img/svgs/search.svg" alt="search icon" /> </button>{/* search button */}
                            </span>

                            <div className="domain-price-header mr-auto">

                                <a>
                                    <img src="img/domain/com.png" alt="domain" />{/* domain name */}
                                    <span>$14.99</span>{/* domain price */}
                                </a>

                                <a>
                                    <img src="img/domain/net.png" alt="domain" />{/* domain name */}
                                    <span>$9.99</span>{/* domain price */}
                                </a>

                                <a>
                                    <img src="img/domain/org.png" alt="domain" />{/* domain name */}
                                    <span>$0.99</span>{/* domain price */}
                                </a>

                                <a className="no-phon-dsply">
                                    <img src="img/domain/store.png" alt="domain" />{/* domain name */}
                                    <span>$8.99</span>{/* domain price */}
                                </a>

                            </div>

                        </form>{/* end main header domain search form */}

                    </div>{/* end col */}
                </div>{/* end row */}
            </div>
        </div>{/* end main header domain search */}
    </div> {/* end header */}

	
	
    <section className="first-items-home">{/* start our features section */}
        <div className="container">{/* start container */}
            <div className="row justify-content-left">{/* row */}
                <div className="col-md-4 item-icons">{/* features box */}
                    <span className="free-badje">free</span>{/* free badge */}
                    <span className="nomber-overlow"><b>01</b></span>
                    <i className="icon flaticon-bug color-1"></i>{/* icon */}
                    <h5>Full Security Team</h5>{/* title */}
                    <p>Solution is an easy to use tool all very easy! eCommerce. With the help of our system you can present.</p>{/* text */}
                    <div className="badje-link-footer"><a href="#">see more  <i className="far fa-arrow-alt-circle-right"></i> </a></div>{/* button */}
                </div>{/* end features box */}

                <div className="col-md-4 item-icons">{/* features box */}
                    <span className="nomber-overlow"><b>02</b></span>
                    <i className="icon flaticon-chip color-2"></i>{/* icon */}
                    <h5>Analytics</h5>{/* title */}
                    <p>Solution is an easy to use tool all very easy! eCommerce. With the help of our system you can present.</p>{/* text */}
                    <div className="badje-link-footer"><a href="#">see more  <i className="far fa-arrow-alt-circle-right"></i> </a></div>{/* button */}
                </div>{/* end features box */}

                <div className="col-md-4 item-icons">{/* features box */}
                    <span className="nomber-overlow"><b>03</b></span>
                    <i className="icon flaticon-063-flashlight color-3"></i>{/* icon */}
                    <h5>Analytics</h5>{/* title */}
                    <p>Solution is an easy to use tool all very easy! eCommerce. With the help of our system you can present.</p>{/* text */}
                    <div className="badje-link-footer"><a href="#">see more  <i className="far fa-arrow-alt-circle-right"></i> </a></div>{/* button */}
                </div>{/* end features box */}

            </div>{/* end row */}
        </div>{/* end container */}
    </section>{/* end our features section */}

	
	
	
    <section className="second-items-home">{/* first hosting plans style */}
        <div className="container">{/* start container */}
            <div className="tittle-simple-one">{/* start title */}
                <h5>Chose your best pricing plan<span>you want custom hosting plan.<br/> No hidden charge.</span></h5>
			</div>{/* end title */}
			
            <div className="row justify-content-center">{/* row */}
                <div className="col-md-3">{/* col */}
                    <div className="pricing-plan-one">{/* start pricing table */}
                        <span className="box-overlow-cari"></span>
                        <h5 className="title-plan"><span>start building your empire</span> basic </h5>
                        <div className="price-palan">
                            <span><i>$</i>75<b>/monthly</b></span>
                        </div>
                        <ul className="body-plan">
                            <li><b>100 MB</b> Disk Space</li>
                            <li><b>Unlimited</b> Bandwith</li>
                            <li><b>standard</b> performance</li>
                            <li>free <b>SSL</b> Certificate</li>
                            <li>free <b>Domain</b></li>
                        </ul>
                        <div className="purshase-plan">
                            <a href="#">purshase</a>{/* button */}
                        </div>
                    </div>{/* end pricing table */}
                </div>{/* end col */}

                <div className="col-md-3">{/* col */}
                    <div className="pricing-plan-one spceale-plan">{/* start pricing table */}
                        <span className="best-selle-tag-plan"><b>best selle</b></span>
                        <span className="box-overlow-cari"></span>
                        <h5 className="title-plan"><span>start building your empire</span> basic </h5>
                        <div className="price-palan">
                            <span><i>$</i>75<b>/monthly</b></span>
                        </div>
                        <ul className="body-plan">
                            <li><b>100 MB</b> Disk Space</li>
                            <li><b>Unlimited</b> Bandwith</li>
                            <li><b>standard</b> performance</li>
                            <li>free <b>SSL</b> Certificate</li>
                            <li>free <b>Domain</b></li>
                        </ul>
                        <div className="purshase-plan">
                            <a href="#">purshase</a>{/* button */}
                        </div>
                    </div>{/* end pricing table */}
                </div>{/* end col */}

                <div className="col-md-3">{/* col */}
                    <div className="pricing-plan-one">{/* start pricing table */}
                        <span className="box-overlow-cari"></span>
                        <h5 className="title-plan"><span>start building your empire</span> basic </h5>
                        <div className="price-palan">
                            <span><i>$</i>75<b>/monthly</b></span>
                        </div>
                        <ul className="body-plan">
                            <li><b>100 MB</b> Disk Space</li>
                            <li><b>Unlimited</b> Bandwith</li>
                            <li><b>standard</b> performance</li>
                            <li>free <b>SSL</b> Certificate</li>
                            <li>free <b>Domain</b></li>
                        </ul>
                        <div className="purshase-plan">
                            <a href="#">purshase</a>{/* button */}
                        </div>
                    </div>{/* end pricing table */}
                </div>{/* end col */}

            </div>
        </div>{/* end container */}
    </section>{/* end hosting plans style */}

    <section className="our-pertners">{/* starts our pertners section */}
        <div className="container">{/* CONTAINER */}
		<h2 className="d-none">our pertners</h2>
            <div className="owl-carousel pertners-carousel owl-theme">{/* start owl carousel */}
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo1.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo2.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo3.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo4.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo5.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo1.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo2.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo3.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo4.png" alt="" /> </a>
				</div>{/* end item */}
				
                <div className="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo5.png" alt="" /> </a>
                </div>{/* end item */}
				
            </div>{/* end owl carousel */}

        </div>{/* end CONTAINER */}
    </section>{/* end our pertners section */}

    <section className="perlex-efect-section parallax-window" data-parallax="scroll" data-image-src="img/demo/bg.jpg">{/* start parallax section */}
        <span className="perlex-hidden-iverlow"></span>
        <div className="container">{/* start container */}
            <div className="row justify-content-left">{/* start row */}
                <div className="col-md-5">
                    <div className="video-section-text-place">
                        <span className="over-ole-grandient-orl"></span>
                        <h5>Chose your best pricing plan</h5>{/* title */}
                        <span className="post-category">hosting</span> <span className="post-date">19 march 2018</span>{/* category */}
                        <p>Solution is an easy to use tool all very easy! eCommerce. With the help of our system you can present.</p>{/* text */}
                        <a href="#">read more</a>{/* link */}
                    </div>
                </div>

                <div className="col-md-7 video-section-play-place">{/* video popup link */}
                    <a className="video-btn" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0" data-target="#videomodal" href="#"><i className="fas fa-play"></i> <span className="first-text"><b>play</b> video</span> <span className="second-text"><b>click</b> here</span></a>
                </div>{/* end popup link */}
            </div>{/* end row */}

        </div>{/* end container */}
    </section>{/* end parallax section */}

    <section className="our-sevices-section">{/* start our full secvices */}
            <div className="container">
                <div className="tittle-simple-one">{/* start title */}
                <h5>Chose your best pricing plan<span>you want custom hosting plan.<br/> No hidden charge.</span></h5>
				</div>{/* end title */}
            </div>{/* end container */}
			
        <ul id="oursevices" className="carousel carousel-nav-services">{/* our full secvices links */}
		
            <li className="carousel-cell">{/* start item */}
                <a className="nav-link">
                    <i className="flaticon-063-atomic"></i>
                    <span className="title-tabs-of">Domains</span>{/* title */}
                </a>
            </li>{/* end item */}

            <li className="carousel-cell">{/* start item */}
                <a className="nav-link">
                    <i className="flaticon-063-circuit"></i>
                    <span className="title-tabs-of">web hosting</span>{/* title */}
                </a>
            </li>{/* end item */}

            <li className="carousel-cell">{/* start item */}
                <a className="nav-link">
                    <i className="flaticon-063-smartphone"></i>
                    <span className="title-tabs-of">resslers</span>{/* title */}
                </a>
            </li>{/* end item */}

            <li className="carousel-cell">{/* start item */}
                <a className="nav-link">
                    <i className="flaticon-063-ufo"></i>
                    <span className="title-tabs-of">support</span>{/* title */}
                </a>
            </li>{/* end item */}

            <li className="carousel-cell">{/* start item */}
                <a className="nav-link">
                    <i className="flaticon-063-test-tube"></i>
                    <span className="title-tabs-of">cloud hosting</span>{/* title */}
                </a>
            </li>{/* end item */}
			
        </ul>{/* end our full secvices links */}

		
		
        <div className="carousel carousel-main-services">{/* tabs show */}
            <div className="carousel-cell">{/* item */}
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8 text-tab-content-algo text-center">
                            <div className="text-absoo">
                                <h5>Shared Web Hosting is your next powerful thing.</h5>{/* title */}
                                <p>With our one click installer tool, available on every Web Hosting plan, you can create any type of website: blog, forum, CMS, wiki, photo gallery, E-commerce store, and so much more! No more thinking for databases, downloading and uploading script files on your own, and other technical work.</p>{/* text */}
                                <a href="#">GET STARTED NOW</a>{/* link */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>{/* end item */}

            <div className="carousel-cell">{/* item */}
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-5">
                            <img src="img/demo/server.jpg" alt="" />
                        </div>
                        <div className="col-md-6 text-tab-content-algo">
                            <div className="text-absoo">
                                <h5>Shared Web Hosting is your next powerful thing.</h5>{/* title */}
                                <p>With our one click installer tool, available on every Web Hosting plan, you can create any type of website: blog, forum, CMS, wiki, photo gallery, E-commerce store, and so much more! No more thinking for databases, downloading and uploading script files on your own, and other technical work.</p>{/* text */}
                                <a href="#">GET STARTED NOW</a>{/* link */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>{/* end item */}
			
            <div className="carousel-cell">{/* item */}
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-5 text-tab-content-algo">
                            <div className="text-absoo">
                                <h5>Shared Web Hosting is your next powerful thing.</h5>{/* title */}
                                <p>With our one click installer tool, available on every Web Hosting plan, you can create any type of website: blog, forum, CMS, wiki, photo gallery, E-commerce store, and so much more! No more thinking for databases, downloading and uploading script files on your own, and other technical work.</p>{/* text */}
                                <a href="#">GET STARTED NOW</a>{/* link */}
                            </div>
                        </div>

                        <div className="col-md-5">
                            <img src="img/demo/server.jpg" alt="" />
                        </div>

                    </div>
                </div>
            </div>{/* end item */}

            <div className="carousel-cell">{/* item */}
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-5">
                            <img src="img/demo/server.jpg" alt="" />
                        </div>
                        <div className="col-md-6 text-tab-content-algo">
                            <div className="text-absoo">
                                <h5>Shared Web Hosting is your next powerful thing.</h5>{/* title */}
                                <p>With our one click installer tool, available on every Web Hosting plan, you can create any type of website: blog, forum, CMS, wiki, photo gallery, E-commerce store, and so much more! No more thinking for databases, downloading and uploading script files on your own, and other technical work.</p>{/* text */}
                                <a href="#">GET STARTED NOW</a>{/* link */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>{/* end item */}
			
            <div className="carousel-cell">{/* item */}
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-5 text-tab-content-algo">
                            <div className="text-absoo">
                                <h5>Shared Web Hosting is your next powerful thing.</h5>{/* title */}
                                <p>With our one click installer tool, available on every Web Hosting plan, you can create any type of website: blog, forum, CMS, wiki, photo gallery, E-commerce store, and so much more! No more thinking for databases, downloading and uploading script files on your own, and other technical work.</p>{/* text */}
                                <a href="#">GET STARTED NOW</a>{/* link */}
                            </div>
                        </div>

                        <div className="col-md-5">
                            <img src="img/demo/server.jpg" alt="" />
                        </div>

                    </div>
                </div>
            </div>{/* end item */}

        </div>{/* end tabs show */}
    </section>{/* end our full secvices */}
	
	
	
	
    <section className="counter-section">{/* start counter section */}
        <div className="container">
            <div className="counter-middle-icon">
                <span className="icon-counter-sid"><i className="flaticon-063-molecules-1"></i></span>{/* icon */}
                <h2 className="icon-counter-sid-title">our company <b>funny facts</b></h2>{/* title */}
            </div>
            <div className="row justify-content-center">{/* row */}
                <div className="col-md-4 counter-number-tibo"><i className="flaticon-database-8"></i> <span><small className="counter">99.99</small>%</span> <b>off our server working now</b></div>{/* counter */}
                <div className="col-md-4 counter-number-tibo pad-top-190"><i className="flaticon-plug"></i><span> <small className="counter">122</small></span><b>new website this week</b></div>{/* counter */}
                <div className="col-md-4 counter-number-tibo"><i className="flaticon-shield-5"></i><span> <small className="counter">973.07</small>$</span> <b>checkout this day</b></div>{/* counter */}
            </div>{/* end row */}

        </div>{/* end container */}
    </section>{/* end counter section */}

    <section className="server-place-section">{/* start our servers place section */}
        <div className="container zindextow">{/* start container */}
            <div className="row justify-content-center">{/* start row */}
                <div className="col-md-5 row justify-content-center text-reve-map-place">
                    <div className="col-md-6">
                        <h5>brazil</h5>{/* title */}
                        <p><span>address</span> tchuk slov 133, contral park,brazil</p>{/* address */}
                        <p><span>phone</span>00213 123-45-67-89 {/* phones */}
                            <br/>00213 123-45-67-89</p>{/* phones */}
                        <p><span>email</span> support@coodiv.net</p>{/* email */}
                    </div>

                    <div className="col-md-6">
                        <h5>paris</h5>{/* title */}
                        <p><span>address</span> tchuk slov 133, contral park,paris</p>{/* address */}
                        <p><span>phone</span>00213 123-45-67-89 {/* phones */}
                            <br/>00213 123-45-67-89</p>{/* phones */}
                        <p><span>email</span> support@coodiv.net</p>{/* email */}
                    </div>
                </div>

                <div className="col-md-7">{/* col */}
                    <div className="map-gene-server-place">{/* start map place */}
                        <img src="img/map/map-world.png" alt="" />{/* map */}
                        <span data-toggle="tooltip" data-placement="top" title="brazil" style={{top: 68, left: 33}} className="place"></span>
                        <span data-toggle="tooltip" data-placement="top" title="paris" style={{top: 35,left: 47}} className="place"></span>
                    </div>{/* end map place */}
                </div>{/* end col */}
				
            </div>{/* end row */}
        </div>{/* end container */}
    </section>{/* end our servers place section */}
	

    <section className="blog-home-section">{/* start blog section */}
        <div className="container">{/* start container */}
		
            <div className="tittle-simple-one"><h5>The latest news in our blog<span>Lorem Ipsum Dolor Sit Amet Lorem<br/> Lorem Ipsum Dolor.</span></h5></div>{/* title */}

            <div className="row justify-content-center blog-items-home">{/* start row */}
			
                <div className="col-md-4">{/* col */}
                    <div className="home-blog-te">{/* blog container */}
                        <div className="post-thumbn parallax-window" style={{background: "url(img/blog/blog4.png) no-repeat center"}}></div>{/* post thumbnail */}
                        <div className="post-bodyn">
                            <h5><a href="#">Up and Running With WooCommerce</a></h5>{/* post title */}
                            <p><i className="far fa-calendar"></i>Mars 02,2018</p>{/* post date */}
                        </div>
                    </div>{/* end blog container */}
                </div>{/* end col */}

                <div className="col-md-4">{/* col */}
                    <div className="home-blog-te">{/* blog container */}
                        <div className="post-thumbn parallax-window" style={{background: "url(img/blog/blog3.png) no-repeat center"}}></div>{/* post thumbnail */}
                        <div className="post-bodyn">
                            <h5><a href="#">Up and Running With WooCommerce</a></h5>{/* post title */}
                            <p><i className="far fa-calendar"></i>Mars 02,2018</p>{/* post date */}
                        </div>
                    </div>{/* end blog container */}
                </div>{/* end col */}

                <div className="col-md-4">{/* col */}
                    <div className="home-blog-te">{/* blog container */}
                        <div className="post-thumbn parallax-window" style={{background: "url(img/blog/blog2.png) no-repeat center"}}></div>{/* post thumbnail */}
                        <div className="post-bodyn">
                            <h5><a href="#">Up and Running With WooCommerce</a></h5>{/* post title */}
                            <p><i className="far fa-calendar"></i>Mars 02,2018</p>{/* post date */}
                        </div>
                    </div>{/* end blog container */}
                </div>{/* end col */}

            </div>{/* end row */}

        </div>{/* end container */}
    </section>{/* end blog section */}

	
	
	
	
    <section className="form-contact-home-section" style={{marginBottom: -10, marginLeft: 2}}>{/* start contact us section */}
        <div className="container">{/* start container */}
            <div className="row justify-content-center">{/* start row */}
                <form className="col-md-8 row justify-content-center form-contain-home" id="ajax-contact" method="post" action="mailer.php">{/* start form */}
                    <h5>Any Project In Your Mind ?<span>Or Just Say Hello</span></h5>{/* title */}
					
					<div id="form-messages"></div>{/* form message */}
					
                    <div className="col-md-6">{/* start col */}
                        <div className="field input-field">
                            <input className="form-contain-home-input" type="text" id="name" name="name" required/>{/* input */}
                            <span className="input-group-prepend">entre your name</span>{/* label */}
                        </div>
                    </div>{/* end col */}
					
                    <div className="col-md-6">{/* start col */}
                        <div className="field input-field">
                            <input className="form-contain-home-input" type="email" id="email" name="email" required />{/* input */}
                            <span className="input-group-prepend">entre your email</span>{/* label */}
                        </div>
                    </div>{/* end col */}
					
                    <div className="col-md-12">{/* start col */}
                        <div className="field input-field">
                            <textarea className="form-contain-home-input" id="message" name="message" required></textarea>{/* textarea */}
                            <span className="input-group-prepend">entre your message</span>{/* label */}
                        </div>
                    </div>{/* end col */}
					
                    <div className="btn-holder-contect">
                        <button type="submit">Send</button>{/* submit button */}
					</div>

                </form>{/* end form */}
            </div>{/* end container */}
		</div>{/* end container */}
    </section>{/* end contact us section */}
    <Footer/>
</body> 
)

export default Header;
