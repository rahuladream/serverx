import React from 'react';
import Footer from './Layout/Footer';
import Menu from './Layout/Menu';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import TopMenu from './Layout/TopMenu';
import FunFact from './Layout/Funfact';
const Home = props => (
    <body>
       {/* start header */}
       <div id="header">
        {/* start search block */}
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>

        </div>
        {/* end search block */}

        {/* start header animations */}
        <div className="header-animation">
            <div id="particles-bg"></div>
            <span className="moon-bg-her"></span>
            {/*----- header support ring -----*/}
            <a className="support-header-ring" href="#"><img src="img/svgs/support.svg" alt="" /> <span>support team</span></a>
            {/*----- end header support ring -----*/}
        </div>
        {/* end header animations */}

		{/* start top header area */}
        <TopMenu />
		{/* end top header area */}
        <Menu />
        <div className="header-heeadline">{/* start main header domain search */}
            <div className="outliner-middl-ha">
                <h5><span className="blok-on-phon">we</span> <span id="typed"></span></h5>{/* main header domain search title */}
                <p>We make sure your website is fast, secure & always up</p>{/* main header domain search text */}

                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <form action="domain-results.html" id="domain-search" className="wow fadeIn" data-wow-delay="0.5s">{/* main header domain search form */}
                            <span className="space-men"></span>
                            <input id="domain-text" type="text" name="domain" placeholder="Write your domain name here.." />{/* search input */}
                            <span className="inline-button">
                            <button id="search-btn" type="submit" name="submit" value="Search"> <img src="img/svgs/search.svg" alt="search icon" /> </button>{/* search button */}
                            </span>

                            <div className="domain-price-header mr-auto">

                                <a>
                                    <img src="img/domain/com.png" alt="domain" />{/* domain name */}
                                    <span>₹659</span>{/* domain price */}
                                </a>

                                <a>
                                    <img src="img/domain/net.png" alt="domain" />{/* domain name */}
                                    <span>₹379</span>{/* domain price */}
                                </a>

                                <a>
                                    <img src="img/domain/org.png" alt="domain" />{/* domain name */}
                                    <span>₹300</span>{/* domain price */}
                                </a>

                                <a className="no-phon-dsply">
                                    <img src="img/domain/store.png" alt="domain" />{/* domain name */}
                                    <span>₹700</span>{/* domain price */}
                                </a>

                            </div>

                        </form>{/* end main header domain search form */}

                    </div>{/* end col */}
                </div>{/* end row */}
            </div>
        </div>{/* end main header domain search */}
    </div> {/* end header */}



    <section className="first-items-home">{/* start our features section */}
        <div className="container">{/* start container */}
            <div className="row justify-content-left">{/* row */}
                <div className="col-md-4 item-icons">{/* features box */}
                    <span className="free-badje">free</span>{/* free badge */}
                    <span className="nomber-overlow"><b>01</b></span>
                    <i className="icon flaticon-bug color-1"></i>{/* icon */}
                    <h5>Unmetered Resources</h5>{/* title */}
                    <p>We provide Unmetered resources to our clients. (i.e. Unlimited bandwidth, Disk space, email accounts, MySQL database etc.)</p>{/* text */}
                    <div className="badje-link-footer"><Link to='/aboutus'>see more  <i className="far fa-arrow-alt-circle-right"></i> </Link></div>{/* button */}
                </div>{/* end features box */}

                <div className="col-md-4 item-icons">{/* features box */}
                    <span className="nomber-overlow"><b>02</b></span>
                    <i className="icon flaticon-chip color-2"></i>{/* icon */}
                    <h5>99.9% Uptime</h5>{/* title */}
                    <p>Our servers provide 99.9% uptime to make sure the smooth working of every website..</p>{/* text */}
                    <div className="badje-link-footer"><Link to='/aboutus'>see more  <i className="far fa-arrow-alt-circle-right"></i> </Link></div>{/* button */}
                </div>{/* end features box */}

                <div className="col-md-4 item-icons">{/* features box */}
                    <span className="nomber-overlow"><b>03</b></span>
                    <i className="icon flaticon-063-flashlight color-3"></i>{/* icon */}
                    <h5>24*7 Technical Support</h5>{/* title */}
                    <p>A team of technical experts is always available to provide assistance to the customers.</p>{/* text */}
                    <div className="badje-link-footer"><Link to='/aboutus'>see more  <i className="far fa-arrow-alt-circle-right"></i> </Link></div>{/* button */}
                </div>{/* end features box */}

            </div>{/* end row */}
        </div>{/* end container */}
    </section>{/* end our features section */}




    <section className="second-items-home">{/* first hosting plans style */}
        <div className="container">{/* start container */}
            <div className="tittle-simple-one">{/* start title */}
                <h5>Choose your best pricing plan<span>you want custom hosting plan.<br/> No hidden charge.</span></h5>
			</div>{/* end title */}

            <div className="row justify-content-center">{/* row */}
                <div className="col-md-3">{/* col */}
                    <div className="pricing-plan-one">{/* start pricing table */}
                        <span className="box-overlow-cari"></span>
                        <h5 className="title-plan"><span>start building your empire</span> starter </h5>
                        <div className="price-palan">
                            <span><i>₹</i>99<b>/monthly</b></span>
                        </div>
                        <ul className="body-plan">
                            <li>Single Domain</li>
                            <li><b>5GB</b> Disk Space</li>
                            <li><b>umetered</b> bandwith</li>
                            <li><b>5 mail</b> account(s)</li>
                            <li><b>5 MYSQL</b> database</li>
                        </ul>
                        <div className="purshase-plan">
                            <a href="#">purchase</a>{/* button */}
                        </div>
                    </div>{/* end pricing table */}
                </div>{/* end col */}

                <div className="col-md-3">{/* col */}
                    <div className="pricing-plan-one spceale-plan">{/* start pricing table */}
                        <span className="best-selle-tag-plan"><b>best seller</b></span>
                        <span className="box-overlow-cari"></span>
                        <h5 className="title-plan"><span>start building your empire</span> standard </h5>
                        <div className="price-palan">
                            <span><i>₹</i>149<b>/monthly</b></span>
                        </div>
                        <ul className="body-plan">
                            <li>Single Domain</li>
                            <li><b>Unlimited</b> Disk</li>
                            <li><b>Unmetered</b> Bandwith</li>
                            <li>free <b>Unlimited</b> Email Account(s)</li>
                            <li><b>Unlimited MYSQL</b> Database</li>
                            <li><b>Free SSL</b> Certificate</li>
                        </ul>
                        <div className="purshase-plan">
                            <a href="#">purchase</a>{/* button */}
                        </div>
                    </div>{/* end pricing table */}
                </div>{/* end col */}

                <div className="col-md-3">{/* col */}
                    <div className="pricing-plan-one">{/* start pricing table */}
                        <span className="box-overlow-cari"></span>
                        <h5 className="title-plan"><span>start building your empire</span> Enterprise </h5>
                        <div className="price-palan">
                            <span><i>₹</i>349<b>/monthly</b></span>
                        </div>
                        <ul className="body-plan">
                            <li><b>Upto 15</b> Domains</li>
                            <li><b>Unlimited</b> Bandwith</li>
                            <li><b>Unlimited</b> Disk Space</li>
                            <li>free <b>Unlimited</b> Email Account(s)</li>
                            <li><b>Unlimited MYSQL</b> Database</li>
                            <li><b>Free SSL</b> Certificate</li>
                        </ul>
                        <div className="purshase-plan">
                            <a href="#">purchase</a>{/* button */}
                        </div>
                    </div>{/* end pricing table */}
                </div>{/* end col */}

            </div>
        </div>{/* end container */}
    </section>{/* end hosting plans style */}

    <section className="our-pertners">{/* starts our pertners section */}
        <div className="container">{/* CONTAINER */}
		<h2 className="d-none">our pertners</h2>
            <div className="owl-carousel pertners-carousel owl-theme">{/* start owl carousel */}


                 <div className="item">{/* start item */}
				
                </div>{/* end item */}
                
                <div className="item">{/* start item */}
				<a href="http://hajaam.in"> <img src="img/sponsor/hajaam.png" alt="" /> </a>
                </div>{/* end item */}

                <div className="item">{/* start item */}
				<a href="http://braddy.info"> <img src="img/sponsor/braddy.png" alt="" /> </a>
                </div>{/* end item */}

                <div className="item">{/* start item */}
				<a href="https://talentfinder.co.in"> <img src="img/sponsor/talentfinder.png" alt="" /> </a>
                </div>{/* end item */}

            </div>{/* end owl carousel */}

        </div>{/* end CONTAINER */}
    </section>{/* end our pertners section */}

    <section className="perlex-efect-section parallax-window" data-parallax="scroll" style={{backgroundImage: `url('img/custom/bg.jpg')`}}>{/* start parallax section */}
        <span className="perlex-hidden-iverlow"></span>
        <div className="container">{/* start container */}
            <div className="row justify-content-left">{/* start row */}
                <div className="col-md-5">
                    <div className="video-section-text-place">
                        <span className="over-ole-grandient-orl"></span>
                        <h5>Choose your best pricing plan</h5>{/* title */}
                        <span className="post-category">hosting</span> <span className="post-date">11 October 2018</span>{/* category */}
                        <p>Its never been easy to find out the best suitable for company, Dont worry We will help you to find out the best plan for your company .</p>{/* text */}
                        <Link to="/best-plan-for-your-company">read more</Link>{/* link */}
                    </div>
                </div>


            </div>{/* end row */}

        </div>{/* end container */}
    </section>{/* end parallax section */}

    <section className="our-sevices-section">{/* start our full secvices */}
            <div className="container">
                <div className="tittle-simple-one">{/* start title */}
                <h5>Choose your best pricing plan<span>you want custom hosting plan.<br/> No hidden charge.</span></h5>
				</div>{/* end title */}
            </div>{/* end container */}

        <ul id="oursevices" className="carousel carousel-nav-services">{/* our full secvices links */}

            <li className="carousel-cell">{/* start item */}
                <a className="nav-link">
                    <i className="flaticon-063-atomic"></i>
                    <span className="title-tabs-of">Domains</span>{/* title */}
                </a>
            </li>{/* end item */}

            <li className="carousel-cell">{/* start item */}
                <a className="nav-link">
                    <i className="flaticon-063-circuit"></i>
                    <span className="title-tabs-of">Web hosting</span>{/* title */}
                </a>
            </li>{/* end item */}

            <li className="carousel-cell">{/* start item */}
                <a className="nav-link">
                    <i className="flaticon-063-smartphone"></i>
                    <span className="title-tabs-of">Shared</span>{/* title */}
                </a>
            </li>{/* end item */}

        </ul>{/* end our full secvices links */}



        <div className="carousel carousel-main-services">{/* tabs show */}
            <div className="carousel-cell">{/* item */}
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8 text-tab-content-algo text-center">
                            <div className="text-absoo">
                                <h5>You are not choosing name , you are creating brand.</h5>{/* title */}
                                <p>we make it easy and inexpensive to get the domain you want, fast. Eco-Friendly Hosting. Free Cloud Storage. Drag & Drop Site Builder. Free Web Applications. Types: .com, .net, .online, .tech, .org, .website, .club, .me, .store, .site.</p>{/* text */}
                                <a href="#">GET STARTED NOW</a>{/* link */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>{/* end item */}

            <div className="carousel-cell">{/* item */}
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-5">
                            <img src="img/custom/scd-img-scd-r901-image3-en-500x500.png" alt="" />
                        </div>
                        <div className="col-md-6 text-tab-content-algo">
                            <div className="text-absoo">
                                <h5>Shaping the future.</h5>{/* title */}
                                <p> Deploy Low-Cost and Reliable Websites in Minutes. Get Started Today. Pay As You Go. Durable, Safe & Secure. Reliable Support. Elastic Scalability. Types: Public Websites, Online Applications, Intranet Sites. </p>
                                <a href="#">GET STARTED NOW</a>{/* link */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>{/* end item */}

            <div className="carousel-cell">{/* item */}
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-5 text-tab-content-algo">
                            <div className="text-absoo">
                                <h5>Shared Web Hosting is your next powerful thing.</h5>{/* title */}
                                <p>With our one click installer tool, available on every Web Hosting plan, you can create any type of website: blog, forum, CMS, wiki, photo gallery, E-commerce store, and so much more! No more thinking for databases, downloading and uploading script files on your own, and other technical work.</p>{/* text */}
                                <a href="#">GET STARTED NOW</a>{/* link */}
                            </div>
                        </div>

                        <div className="col-md-5">
                            <img src="img/custom/sharedHOsting.png" style={{marginTop: 50}} alt="" />
                        </div>

                    </div>
                </div>
            </div>{/* end item */}

        

        </div>{/* end tabs show */}
    </section>{/* end our full secvices */}

    <FunFact />

    <section className="form-contact-home-section" style={{marginBottom: -10, marginLeft: 2}}>{/* start contact us section */}
        <div className="container">{/* start container */}
            <div className="row justify-content-center">{/* start row */}
                <form className="col-md-8 row justify-content-center form-contain-home" id="ajax-contact" method="post" action="mailer.php">{/* start form */}
                    <h5>Any Startup In Your Mind ?<span>Or Just Say Hello</span></h5>{/* title */}

					       <div id="form-messages"></div>{/* form message */}
                    <div className="col-md-6">{/* start col */}
                        <div className="field input-field">
                            <input className="form-contain-home-input" type="text" id="name" name="name" required/>{/* input */}
                            <span className="input-group-prepend">entre your name</span>{/* label */}
                        </div>
                    </div>{/* end col */}

                    <div className="col-md-6">{/* start col */}
                        <div className="field input-field">
                            <input className="form-contain-home-input" type="email" id="email" name="email" required />{/* input */}
                            <span className="input-group-prepend">entre your email</span>{/* label */}
                        </div>
                    </div>{/* end col */}

                    <div className="col-md-12">{/* start col */}
                        <div className="field input-field">
                            <textarea className="form-contain-home-input" id="message" name="message" required></textarea>{/* textarea */}
                            <span className="input-group-prepend">entre your message</span>{/* label */}
                        </div>
                    </div>{/* end col */}

                    <div className="btn-holder-contect">
                        <button type="submit">Send</button>{/* submit button */}
					</div>

                </form>{/* end form */}
            </div>{/* end container */}
		</div>{/* end container */}
    </section>{/* end contact us section */}
    <Footer/>
</body>
)

export default Home;
