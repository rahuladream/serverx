import React from 'react';
import Menu from '../Layout/Menu';
import Footer from '../Layout/Footer';

class Blog extends React.Component {
    render(){
        return(
            <body>
            <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-headerv2">
               <div className="search-header-block">
                   <form id="pitursrach-header" name="formsearch" method="get" action="#">
                       <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                       <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                       <a className="closesrch-her-block np-dsp-block">
                           <span className="first-stright"></span>
                           <span className="second-stright"></span>
                       </a>
                   </form>
               </div>
               <div className="header-animation">
                   <div id="particles-bg"></div>
               </div>
               <Menu />
               <div className="mt-auto"></div>
           </div>
        

	<section class="padding-60-0-100">
	<div class="container the_breadcrumb_conatiner_page">
    <div class="the_breadcrumb">
    <div class="breadcrumbs"><i class="fas fa-home"></i><a href="index.html">Nuhost</a> / All Posts</div>
	</div>
    </div>
	
	<div class="container blog-container-page">
	<div class="row justify-content-left mr-tp-60">
	<div class="col-md-8">
	
	<div class="home-blog-te special-in-blog-page">{/* blog container */}
    <div class="post-thumbn parallax-window" style={{background: "url(img/blog/blog4.png) no-repeat center"}}></div>{/* post thumbnail */}
    <div class="post-bodyn">
    <h5><a href="blog-single.html">Up and Running With WooCommerce</a></h5>{/* post title */}
	<p class="post-bodyn-text">is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
    <p><i class="far fa-calendar"></i>Mars 02,2018</p>{/* post date */}
    </div>
    </div>{/* end blog container */}

	
	<div class="home-blog-te special-in-blog-page">{/* blog container */}
    <div class="post-thumbn parallax-window" style={{background: "url(img/blog/first.jpg) no-repeat center"}}></div>{/* post thumbnail */}
    <div class="post-bodyn">
    <h5><a href="blog-single.html">Up and Running With WooCommerce</a></h5>{/* post title */}
	<p class="post-bodyn-text">is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
    <p><i class="far fa-calendar"></i>Mars 02,2018</p>{/* post date */}
    </div>
    </div>{/* end blog container */}
	
	
	<div class="home-blog-te special-in-blog-page">{/* blog container */}
    <div class="post-thumbn parallax-window" style={{background: "url(img/blog/blog3.png) no-repeat center"}}></div>{/* post thumbnail */}
    <div class="post-bodyn">
    <h5><a href="blog-single.html">Up and Running With WooCommerce</a></h5>{/* post title */}
	<p class="post-bodyn-text">is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
    <p><i class="far fa-calendar"></i>Mars 02,2018</p>{/* post date */}
    </div>
    </div>{/* end blog container */}


    <div class="home-blog-te special-in-blog-page">{/* blog container */}
    <div class="post-thumbn parallax-window" style={{background: "url(img/blog/blog2.png) no-repeat center"}}></div>{/* post thumbnail */}
    <div class="post-bodyn">
    <h5><a href="blog-single.html">Up and Running With WooCommerce</a></h5>{/* post title */}
	<p class="post-bodyn-text">is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
    <p><i class="far fa-calendar"></i>Mars 02,2018</p>{/* post date */}
    </div>
    </div>{/* end blog container */}	


    <nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center pagination-nuhost">
    <li class="page-item disabled">
    <a class="page-link" href="#" tabindex="-1"><i class="fas fa-angle-left"></i></a>
    </li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
    <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>
    </li>
    </ul>
    </nav>
  
	</div>
	
	<aside class="col-md-4 blog-sidebar">
          
		  <form class="coodiv-serchform" method="get" id="searchform" action="404.html" role="search">
		    <div class="input-group">
		    <input class="field form-control" id="s" name="s" type="text" placeholder="Search …"/>
		    <button type="submit" id="searchsubmit"><i class="fa fa-search" aria-hidden="true"></i></button>
		    </div>
		  </form>
		  
          <div class="blog-aside-widget">
            <h4 class="blog-aside-widget-title">Archives</h4>
            <ol class="list-unstyled mb-0">
              <li><a href="#">March 2014</a></li>
              <li><a href="#">February 2014</a></li>
              <li><a href="#">January 2014</a></li>
              <li><a href="#">December 2013</a></li>
              <li><a href="#">November 2013</a></li>
              <li><a href="#">October 2013</a></li>
              <li><a href="#">September 2013</a></li>
              <li><a href="#">August 2013</a></li>
              <li><a href="#">July 2013</a></li>
              <li><a href="#">June 2013</a></li>
              <li><a href="#">May 2013</a></li>
              <li><a href="#">April 2013</a></li>
            </ol>
          </div>

          <div class="blog-aside-widget">
            <h4 class="blog-aside-widget-title">Our links</h4>
            <ol class="list-unstyled">
              <li><a href="#">GitHub</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Facebook</a></li>
            </ol>
          </div>
		  
		  <div class="blog-aside-widget meta">
            <h4 class="blog-aside-widget-title">Meta</h4>
            <ol class="list-unstyled">
              <li><a href="#">LOG IN</a></li>
              <li><a href="#">ENTRIES RSS</a></li>
              <li><a href="#">COMMENTS RSS </a></li>
            </ol>
          </div>
		  
    </aside>{/* /.blog-sidebar */}
		
		
	</div>
	</div>
	</section>
	

        <Footer/>
       </body>  
        )
    }
}
export default Blog;