import React from 'react';
import Header from './Layout/Header';
import Footer from './Layout/Footer';
import Menu from './Layout/Menu';
import Faq from './Faq';
import MainFeature from './MainFeature';

class WebHosting extends React.Component {
    render() {
        return(
            <body>
     <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-header">
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>
        </div>
        <div className="header-animation">
        <div id="particles-bg"></div>
            <span className="courve-gb-hdr-top"></span>
            <a className="support-header-ring" href="#"><img src="img/svgs/support.svg" alt="" /> <span>support team</span></a>
        </div>
        <Menu />
        
        <main className="inner cover header-heeadline-title mb-auto">
        <h5><span className="blok-on-phon">web hosting</span></h5>
        <p>For Startups, Companies, Designers and Developers</p>
        </main>

        <div className="mt-auto"></div>
    </div>
   
	<section class="padding-100-0">
	<div class="container">{/* start container */}
            <div class="mr-tp-20">
                <div class="row">
                    <div id="monthly-yearly-chenge" class="col align-self-start">
                        {/* start plan price changer */}
                        <a class="active monthly-price">monthly price</a>
                        <a class="yearli-price">yearly price</a>
                    </div>
                    {/* end plan price changer */}

                    <div class="col align-self-end plan-contact-support-button text-right">
                        {/* start plan support */}
                        <a href="#"><i class="far fa-life-ring"></i>  70 6011 5192 </a>
                    </div>
                    {/* start plan support */}
                </div>

                <div class="row justify-content-left">
                    {/*------------------------------------------- first plan -------------------------------------------*/}
                    <div class="col-md-4">
                        {/* tree steps hosting plan */}
                        <div class="tree-steps-hosting-plans first">
                            <div class="tree-steps-hosting-plans-header">
                                <i class="fas fa-fire tree-steps-hosting-plans-icon"></i>
                                <h5 class="tree-steps-hosting-plans-title">starter plan <small>for small projects</small></h5>
                                {/* steps hosting plan title */}
                                <span class="tree-steps-hosting-plans-price monthly">{/* steps hosting plan price */}
	                            <b class="monthly">₹ 99<small>/month</small></b>
                            	<b class="yearly">₹ {99*12}<small>/year</small></b>
                            	</span>
                                {/* steps hosting plan price */}
                            </div>

                            <div id="first-plan-hosting-steps-content" class="tree-steps-hosting-plans-body first-plan-hosting-steps">
                                {/* steps hosting plan body */}
                                <div class="loader-tree-steps-hosting-plans-body">
                                    {/* steps hosting plan loader */}
                                    <i class="fas fa-circle-notch rotate360icon"></i>
                                </div>
                                {/* end steps hosting plan loader */}
                                <ul class="tree-steps-hosting-plans-list">
                                    {/* steps hosting plan features list */}
                                    <li class="checked">2 TB of space</li>
                                    <li class="checked">unlimited bandwidth</li>
                                    <li class="checked">full backup systems</li>
                                    <li class="checked">free domain</li>
                                    <li class="checked">unlimited database</li>
                                    <li class="not-checked">live chat support</li>
                                    <li class="not-checked">phone support</li>
                                </ul>
                                {/* end steps hosting plan features list */}

                                <div class="tree-steps-hosting-plans-payment">
                                    {/* steps hosting plan login form */}

                                    {/* back to previous steps button */}
                                    <span id="first-plan-hosting" data-toggle="tooltip" data-placement="bottom" title="previous step" class="tree-steps-hosting-plans-footer-btn-back step-two-hosting">	
                                 	<span class="first-stright"></span>
                                    <span class="second-stright"></span>
                                    </span>
                                    {/* end back to previous steps button */}

                                    <h5 class="tree-steps-hosting-plans-payment-title">payment <small>please login to your account to continue </small></h5>

                                    <form class="tree-steps-hosting-plans-login-form" novalidate>
                                        <div class="col-md-12 mb-3">
                                            <label for="firstName">First name</label>
                                            <input type="text" class="form-control" id="firstName" placeholder="" value="" required/>
                                        </div>

                                        <div class="col-md-12 mb-3">
                                            <label for="password">password</label>
                                            <input type="password" class="form-control" id="password" placeholder="" value="" required/>
                                        </div>

                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1"/>
                                            <label class="custom-control-label" for="customCheck1"><small>Remember Me</small></label>
                                        </div>

                                    </form>

                                </div>
                                {/* end steps hosting plan login form */}

                            </div>
                            {/* end steps hosting plan body */}

                            <div class="tree-steps-hosting-plans-footer text-center">
                                {/* start steps hosting plan footer */}
                                <a id="first-plan-hosting-steps" class="tree-steps-hosting-plans-footer-btn first-step-hosting">
                                    {/* go to previous next step button */}
                                    <span class="first-step-hosting-text">
	                                <small>next step</small>
	                                <i class="fas fa-angle-right"></i>
	                                </span>
                                    {/* end go to previous next step button */}

                                    {/* login button */}
                                    <span class="second-step-hosting-text">
                            	    login
                            	    </span>
                                    {/* end login button */}
                                </a>

                            </div>
                            {/* end steps hosting plan footer */}

                        </div>
                    </div>

                    {/*------------------------------------------- second plan -------------------------------------------*/}
                    <div class="col-md-4">
                        {/* tree steps hosting plan */}
                        <div class="tree-steps-hosting-plans second">
                            <span class="tree-steps-hosting-plans-best">best plan</span>
                            <div class="tree-steps-hosting-plans-header">
                                <i class="fas fa-fire tree-steps-hosting-plans-icon"></i>
                                <h5 class="tree-steps-hosting-plans-title">advanced plan <small>for small Business</small></h5>
                                {/* steps hosting plan title */}
                                <span class="tree-steps-hosting-plans-price monthly">{/* steps hosting plan price */}
	                            <b class="monthly">₹ 199<small>/month</small></b>
	                            <b class="yearly">₹ {199*12}<small>/year</small></b>
	                            </span>
                                {/* steps hosting plan price */}
                            </div>

                            <div id="second-plan-hosting-steps-content" class="tree-steps-hosting-plans-body">
                                {/* steps hosting plan body */}
                                <div class="loader-tree-steps-hosting-plans-body">
                                    {/* steps hosting plan loader */}
                                    <i class="fas fa-circle-notch rotate360icon"></i>
                                </div>
                                {/* end steps hosting plan loader */}
                                <ul class="tree-steps-hosting-plans-list">
                                    {/* steps hosting plan features list */}
                                    <li class="checked">2 TB of space</li>
                                    <li class="checked">unlimited bandwidth</li>
                                    <li class="checked">full backup systems</li>
                                    <li class="checked">free domain</li>
                                    <li class="checked">unlimited database</li>
                                    <li class="checked">live chat support</li>
                                    <li class="not-checked">phone support</li>
                                </ul>
                                {/* end steps hosting plan features list */}

                                <div class="tree-steps-hosting-plans-payment">
                                    {/* steps hosting plan login form */}

                                    {/* back to previous steps button */}
                                    <span id="second-plan-hosting" data-toggle="tooltip" data-placement="bottom" title="previous step" class="tree-steps-hosting-plans-footer-btn-back step-two-hosting">	
                                 	<span class="first-stright"></span>
                                    <span class="second-stright"></span>
                                    </span>
                                    {/* end back to previous steps button */}

                                    <h5 class="tree-steps-hosting-plans-payment-title">payment <small>choose your payment method </small></h5>

                                    <form class="tree-steps-hosting-plans-login-form payment-method-form" novalidate="">
                                        <div class="row with-enic-padding">

                                            <div class="custom-control custom-radio col-md-6 mb-3">
                                                <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required/>
                                                <label class="custom-control-label" for="paypal"><img src="img/demo/paypal.png" alt="" /></label>
                                            </div>

                                            <div class="custom-control custom-radio col-md-6 mb-3">
                                                <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required/>
                                                <label class="custom-control-label" for="credit"><img src="img/demo/card.png" alt="" /></label>
                                            </div>

                                        </div>
                                        <br/>
                                        <div class="row with-enic-padding-twni">
                                            <div class="col-md-12 mb-3">
                                                <label for="cc-number">Credit card number</label>
                                                <input type="text" class="form-control" id="cc-number" placeholder="" required/>
                                            </div>
                                        </div>
                                        <div class="row with-enic-padding-twni">
                                            <div class="col-md-4 mb-3">
                                                <label for="cc-expiration">Expiration</label>
                                                <input type="text" class="form-control" id="cc-expiration" placeholder="" required/>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-3 mb-3">
                                                <label for="cc-expiration">CVV</label>
                                                <input type="text" class="form-control" id="cc-cvv" placeholder="" required/>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                {/* end steps hosting plan login form */}

                            </div>
                            {/* end steps hosting plan body */}

                            <div class="tree-steps-hosting-plans-footer text-center">
                                {/* start steps hosting plan footer */}

                                <a id="second-plan-hosting-steps" class="tree-steps-hosting-plans-footer-btn first-step-hosting">
                                    {/* go to previous next step button */}
                                    <span class="first-step-hosting-text">
	                            <small>next step</small>
                            	<i class="fas fa-angle-right"></i>
                            	</span>
                                    {/* end go to previous next step button */}

                                    {/* login button */}
                                    <span class="second-step-hosting-text">
	                                checkout
	                                </span>
                                    {/* end login button */}
                                </a>

                            </div>
                            {/* end steps hosting plan footer */}

                        </div>
                    </div>

                    {/*------------------------------------------- third plan -------------------------------------------*/}
                    <div class="col-md-4">
                        {/* tree steps hosting plan */}
                        <div class="tree-steps-hosting-plans third">
                            <div class="tree-steps-hosting-plans-header">
                                <i class="fas fa-fire tree-steps-hosting-plans-icon"></i>
                                <h5 class="tree-steps-hosting-plans-title">entreprise plan <small>for big projects</small></h5>
                                {/* steps hosting plan title */}
                                <span class="tree-steps-hosting-plans-price monthly">{/* steps hosting plan price */}
	                            <b class="monthly">₹ 999<small>/month</small></b>
                            	<b class="yearly">₹ {999*12}<small>/year</small></b>
	                            </span>
                                {/* steps hosting plan price */}
                            </div>

                            <div id="third-plan-hosting-steps-content" class="tree-steps-hosting-plans-body">
                                {/* steps hosting plan body */}
                                <div class="loader-tree-steps-hosting-plans-body">
                                    {/* steps hosting plan loader */}
                                    <i class="fas fa-circle-notch rotate360icon"></i>
                                </div>
                                {/* end steps hosting plan loader */}
                                <ul class="tree-steps-hosting-plans-list">
                                    {/* steps hosting plan features list */}
                                    <li class="checked">2 TB of space</li>
                                    <li class="checked">unlimited bandwidth</li>
                                    <li class="checked">full backup systems</li>
                                    <li class="checked">free domain</li>
                                    <li class="checked">unlimited database</li>
                                    <li class="checked">live chat support</li>
                                    <li class="checked">phone support</li>
                                </ul>
                                {/* end steps hosting plan features list */}

                                <div class="tree-steps-hosting-plans-payment">
                                    {/* steps hosting plan login form */}

                                    {/* back to previous steps button */}
                                    <span id="third-plan-hosting" data-toggle="tooltip" data-placement="bottom" title="previous step" class="tree-steps-hosting-plans-footer-btn-back step-two-hosting">	
	                                <span class="first-stright"></span>
                                    <span class="second-stright"></span>
                                    </span>
                                    {/* end back to previous steps button */}

                                    <h5 class="tree-steps-hosting-plans-payment-title">error <small>this offre was ended </small></h5>

                                    <div class="tree-steps-hosting-plans-error">
                                        <i class="far fa-times-circle"></i>
                                        <p>we are sorry, this offre was ended </p>
                                    </div>

                                </div>
                                {/* end steps hosting plan login form */}

                            </div>
                            {/* end steps hosting plan body */}

                            <div class="tree-steps-hosting-plans-footer text-center">
                                {/* start steps hosting plan footer */}

                                <a id="third-plan-hosting-steps" class="tree-steps-hosting-plans-footer-btn first-step-hosting">
                                    {/* go to previous next step button */}
                                    <span class="first-step-hosting-text">
                                 	<small>next step</small>
                                	<i class="fas fa-angle-right"></i>
                                  	</span>
                                    {/* end go to previous next step button */}

                                    {/* login button */}
                                    <span class="second-step-hosting-text">
                                	contact us
                                	</span>
                                    {/* end login button */}
                                </a>

                            </div>
                            {/* end steps hosting plan footer */}

                        </div>
                    </div>

                </div>
            </div>
				<div class="row mr-tp-70">
	<div class="col-md-4 reseller-hosting-features">
	<a href="#">
	<div class="reseller-hosting-features-icon">
	<i class="fas fa-dollar-sign"></i>
	</div>
    <div class="reseller-hosting-features-comments">
	<span class="reseller-hosting-features-title">30days money back</span>
	{/* <span class="reseller-hosting-features-text">is simply dummy text of the printing </span> */}
	</div>
	</a>
	</div>
	
	
	<div class="col-md-4 reseller-hosting-features">
	<a href="#">
	<div class="reseller-hosting-features-icon">
	<i class="fas fa-check"></i>
	</div>
    <div class="reseller-hosting-features-comments">
	<span class="reseller-hosting-features-title">secure payment transaction</span>
	{/* <span class="reseller-hosting-features-text">is simply dummy text of the printing </span> */}
	</div>
	</a>
	</div>
	
	
	<div class="col-md-4 reseller-hosting-features">
	<a href="#">
	<div class="reseller-hosting-features-icon">
	<i class="far fa-life-ring"></i>
	</div>
    <div class="reseller-hosting-features-comments">
	<span class="reseller-hosting-features-title">fast support responding</span>
	{/* <span class="reseller-hosting-features-text">is simply dummy text of the printing </span> */}
	</div>
	</a>
	</div>
	
	
	</div>
        </div>{/* end container */}
	</section>
	
	
	<MainFeature />
	
	
	<section class="our-pertners">{/* starts our pertners section */}
        <div class="container">{/* CONTAINER */}
		<h2 class="d-none">our pertners</h2>
            <div class="owl-carousel pertners-carousel owl-theme">{/* start owl carousel */}
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo1.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo2.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo3.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo4.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo5.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo1.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo2.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo3.png" alt="" /> </a>
                </div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo4.png" alt="" /> </a>
				</div>{/* end item */}
				
                <div class="item">{/* start item */}
				<a href="#"> <img src="img/pertners/logo5.png" alt="" /> </a>
                </div>{/* end item */}
				
            </div>{/* end owl carousel */}

        </div>{/* end CONTAINER */}
    </section>{/* end our pertners section */}
	
	
    <Faq />
	<Footer/>
</body>
        );
    }
}

export default WebHosting;