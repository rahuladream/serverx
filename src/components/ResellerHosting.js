import React from 'react';
import Header from './Layout/Header';
import Footer from './Layout/Footer';
import Menu from './Layout/Menu';
import MainFeature from './MainFeature';
import Faq from './Faq';
class ResellerHosting extends React.Component {
    render() {
        return(
            <body>
     <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-header">
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>
        </div>
        <div className="header-animation">
        <div id="particles-bg"></div>
            <span className="courve-gb-hdr-top"></span>
            <a className="support-header-ring" href="#"><img src="img/svgs/support.svg" alt="" /> <span>support team</span></a>
        </div>
        <Menu />
        
        <main className="inner cover header-heeadline-title mb-auto">
        <h5><span className="blok-on-phon">reseller hosting</span></h5>
        <p>Your Business, Our Products</p>
        </main>

        <div className="mt-auto"></div>
    </div>
    <section class="padding-100-0">
  <div class="container">
  <h2 class="d-none">our plans</h2>
  <div class="row justify-content-center">
  <div class="col-md-5">
  <div class="reseller-hosting-plan-pack first-plans-circels">
  <div class="reseller-hosting-plan-client-selected">
  <i class="fas fa-user-circle" data-toggle="tooltip" data-placement="bottom" title="1.6k client"></i>
  <i class="fas fa-dollar-sign" data-toggle="tooltip" data-placement="bottom" title="30days money back"></i>
  <i class="fas fa-box" data-toggle="tooltip" data-placement="bottom" title="fast delevry"></i>
  <i class="fab fa-cc-visa" data-toggle="tooltip" data-placement="bottom" title="visa accepted"></i>
  </div>
  <div class="reseller-hosting-plan-title">X1 Reseller<span>small projects</span></div>
  <div class="reseller-hosting-plan-price">₹ {249*12} <span>yearly/ttc</span></div>
  <ul class="reseller-hosting-plan-list">
  <li>2 TB of space</li>
  <li>unlimited bandwidth</li>
  <li>Full backup systems</li>
  <li>Unlimited database</li>
  <li class="not-checked"><s>Free Phone Support</s></li>
  </ul>
  
  <p class="reseller-hosting-plan-order"><a href="#">order now</a></p>
  </div>
  </div>
  
  <div class="col-md-5">
  
  <div class="reseller-hosting-plan-pack first-plans-circels dark-ressler-plan">
  <div class="reseller-hosting-plan-client-selected">
  <i class="fas fa-user-circle" data-toggle="tooltip" data-placement="bottom" title="1.6k client"></i>
  <i class="fas fa-dollar-sign" data-toggle="tooltip" data-placement="bottom" title="30days money back"></i>
  <i class="fas fa-box" data-toggle="tooltip" data-placement="bottom" title="fast delevry"></i>
  <i class="fab fa-cc-visa" data-toggle="tooltip" data-placement="bottom" title="visa accepted"></i>
  </div>
  <div class="reseller-hosting-plan-title">X2 Reseller <span>normal projects (+22%)</span></div>
  <div class="reseller-hosting-plan-price">₹ {449*12} <span>yearly/ttc</span> <a href="#">order now</a></div>
  <div class="reseller-hosting-plan-rating"><i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star not-checked"></i> <i class="fas fa-star not-checked"></i> </div>
  </div>
  
  
  <div class="reseller-hosting-plan-pack first-plans-circels dark-ressler-plan mr-tp-30">
  <div class="reseller-hosting-plan-client-selected">
  <i class="fas fa-user-circle" data-toggle="tooltip" data-placement="bottom" title="1.6k client"></i>
  <i class="fas fa-dollar-sign" data-toggle="tooltip" data-placement="bottom" title="30days money back"></i>
  <i class="fas fa-box" data-toggle="tooltip" data-placement="bottom" title="fast delevry"></i>
  <i class="fab fa-cc-visa" data-toggle="tooltip" data-placement="bottom" title="visa accepted"></i>
  </div>
  <div class="reseller-hosting-plan-title">X3 Reseller <span>big projects  (+59%)</span></div>
  <div class="reseller-hosting-plan-price">₹ {749*12} <span>yearly/ttc</span> <a href="#">order now</a></div>
  <div class="reseller-hosting-plan-rating"><i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star-half"></i> </div>
  </div>
  
  
  </div>
  </div>
  
  <div class="row mr-tp-70">
  <div class="col-md-4 reseller-hosting-features">
  <a href="#">
  <div class="reseller-hosting-features-icon">
  <i class="fas fa-dollar-sign"></i>
  </div>
    <div class="reseller-hosting-features-comments">
  <span class="reseller-hosting-features-title">30days money back</span>
  {/* <span class="reseller-hosting-features-text">is simply dummy text of the printing </span> */}
  </div>
  </a>
  </div>
  
  
  <div class="col-md-4 reseller-hosting-features">
  <a href="#">
  <div class="reseller-hosting-features-icon">
  <i class="fas fa-check"></i>
  </div>
    <div class="reseller-hosting-features-comments">
  <span class="reseller-hosting-features-title">secure payment transaction</span>
  {/* <span class="reseller-hosting-features-text">is simply dummy text of the printing </span> */}
  </div>
  </a>
  </div>
  
  
  <div class="col-md-4 reseller-hosting-features">
  <a href="#">
  <div class="reseller-hosting-features-icon">
  <i class="far fa-life-ring"></i>
  </div>
    <div class="reseller-hosting-features-comments">
  <span class="reseller-hosting-features-title">fast support responding</span>
  {/* <span class="reseller-hosting-features-text">is simply dummy text of the printing </span> */}
  </div>
  </a>
  </div>
  
  
  </div>
  </div>
  </section>
  
  
  <MainFeature />
  
  <Faq />
	<Footer/>
</body>
        );
    }
}

export default ResellerHosting;