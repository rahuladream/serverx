import React from 'react';
import Header from './Layout/Header';
import Footer from './Layout/Footer';
import Menu from './Layout/Menu';
import Faq from './Faq';
class WordpressHosting extends React.Component {
    render() {
        return(
            <body>
     <div id="header" className="homepagetwostyle d-flex mx-auto flex-column not-index-header">
        <div className="search-header-block">
            <form id="pitursrach-header" name="formsearch" method="get" action="#">
                <input name="s" id="search" type="text" className="text" value="" placeholder="START TYPING AND PRESS ENTER TO SEARCH" />
                <button type="submit" className="submit"><span className="fa fa-search"></span></button>
                <a className="closesrch-her-block np-dsp-block">
                    <span className="first-stright"></span>
                    <span className="second-stright"></span>
                </a>
            </form>
        </div>
        <div className="header-animation">
        <div id="particles-bg"></div>
            <span className="courve-gb-hdr-top"></span>
            <a className="support-header-ring" href="#"><img src="img/svgs/support.svg" alt="" /> <span>support team</span></a>
        </div>
        <Menu />
        
       
        <main className="inner cover header-heeadline-title mb-auto">

         <h5><span className="blok-on-phon">WordPress hosting</span></h5>
		 <p>Hosting for Startups, Bloggers, Developers</p>
		
		 
        </main>

        <div className="mt-auto"></div>
    </div>
    <section class="padding-100-0">
	<div class="container">
	<div class="row">
    <div class="col-md-12 text-center about-us-page-title">
    <h6 class="about-us-page-title-title"> RECOMONDED BY WORDPRESS.ORG </h6>
    <h2 class="about-us-page-title-sub-title">Best Price! <br/>Best Performance !</h2>
    <p class="about-us-page-title-text"> ServerX provides the State of the Art WordPress hosting along with unmetered resources of our top performing servers.</p>
	</div>
    </div>
	
	<div class="row host-wp-bg-area-conainer position-relative justify-content-between">
	<span class="host-wp-bg-area"></span>
	<div class="col-md-4 mr-tp-100">
	
	<div class="wp-plan-features-box left-side-box">
	<div class="wp-plan-features-number">01</div>
	<div class="wp-plan-features-text">1 click installation </div>
	</div>
	
	<div class="wp-plan-features-box second-left left-side-box">
	<div class="wp-plan-features-number">02</div>
	<div class="wp-plan-features-text">Access to thousands of free themes and plugins</div>
	</div>
	
	<div class="wp-plan-features-box second-left left-side-box">
	<div class="wp-plan-features-number">03</div>
	<div class="wp-plan-features-text">Unlimited Bandwidth/Transfer </div>
	</div>
	
	<div class="wp-plan-features-box left-side-box">
	<div class="wp-plan-features-number">04</div>
	<div class="wp-plan-features-text">Unlimited Disk Space </div>
	</div>
	
	</div>
	
	
	
	
	<div class="col-md-4 mr-tp-100 no-phone-display">
	
	<div class="wp-plan-features-box">
	<div class="wp-plan-features-number">05</div>
	<div class="wp-plan-features-text">Unlimited Business Emails Accounts</div>
	</div>
	
	<div class="wp-plan-features-box second-right">
	<div class="wp-plan-features-number">06</div>
	<div class="wp-plan-features-text">Supersonic Support  </div>
	</div>
	
	<div class="wp-plan-features-box second-right">
	<div class="wp-plan-features-number">07</div>
	<div class="wp-plan-features-text">Get Free Access to SEO tools </div>
	</div>
	
	<div class="wp-plan-features-box">
	<div class="wp-plan-features-number">08</div>
	<div class="wp-plan-features-text">99.9% Uptime </div>
	</div>
	
	</div>
	</div>
	<div class="row justify-content-center">
	<a class="btn-order-default-nuhost" href="#">get a wordpress website now</a>
	</div>
	
	<div class="row mr-tp-70">
	<div class="col-md-4 reseller-hosting-features">
	<a href="#">
	<div class="reseller-hosting-features-icon">
	<i class="fas fa-dollar-sign"></i>
	</div>
    <div class="reseller-hosting-features-comments">
	<span class="reseller-hosting-features-title">Lowest Cost Guarantee </span>
	<span class="reseller-hosting-features-text">ServerX provides services at the lowest price in the world with high-end performance </span>
	</div>
	</a>
	</div>
	
	
	<div class="col-md-4 reseller-hosting-features">
	<a href="#">
	<div class="reseller-hosting-features-icon">
	<i class="fas fa-check"></i>
	</div>
    <div class="reseller-hosting-features-comments">
	<span class="reseller-hosting-features-title">Secure payment Methods</span>
	<span class="reseller-hosting-features-text">ServerX Accepts all payment methods so that customer can make payments easily</span>
	</div>
	</a>
	</div>
	
	
	<div class="col-md-4 reseller-hosting-features">
	<a href="#">
	<div class="reseller-hosting-features-icon">
	<i class="far fa-life-ring"></i>
	</div>
    <div class="reseller-hosting-features-comments">
	<span class="reseller-hosting-features-title">Supersonic Support </span>
	<span class="reseller-hosting-features-text">ServerX have dedicated team to provide support </span>
	</div>
	</a>
	</div>
	
	
	</div>
	
	</div>
	</section>
	
	
	
	
	<section class="padding-130-0-160 gray-background">
	<div class="container">
	<div class="tittle-simple-one"><h5>Why WordPress?<span> WordPress is the world’s most popular website building tool which creates websites, blogs, portfolio etc. without writing a single line of code. It gives you access to thousands of free themes and plugins that help in the design process and allow you to add powerful features and function with just a few clicks. Plus, with ServerX Managed WordPress hosting you get an Unmetered speed that helps to fly website over the internet. </span></h5></div>{/* title */}
    <div class="row justify-content-center mr-tp-60">
	
	<div class="col-md-3 transfer-wp-box fr">
	<div class="transfer-wp-box-icon">
	<span class="number-colection">1</span>
	<i class="fas fa-lock grandient-blue-text-color"></i>
	</div>
	<h5>Buy WordPress Hosting</h5>
	</div>
	
	<div class="col-md-3 transfer-wp-box tw">
	<div class="transfer-wp-box-icon">
	<span class="number-colection">2</span>
	<i class="fas fa-undo grandient-red-text-color"></i>
	</div>
	<h5>Install Themes and Plugin</h5>
	</div>
	
	
	<div class="col-md-3 transfer-wp-box tr">
	<div class="transfer-wp-box-icon">
	<span class="number-colection">3</span>
	<i class="fas fa-hand-pointer grandient-green-text-color"></i>
	</div>
	<h5>Click and Go Fly!</h5>
	</div>
	
	
	</div>
	
	</div>
	</section>
	
	
	{/* <section class="padding-100-0">
	<div class="container">
	<div class="tittle-simple-one"><h5>What you get with every plan<span>is simply dummy text of the printing.<br/> typesetting industry.</span></h5></div>title */}

	{/* <div class="row mr-tp-50">
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fab fa-amazon-pay grandient-blue-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-bug grandient-red-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-database grandient-green-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fab fa-expeditedssl grandient-yellow-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-gem grandient-blue-t-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	<div class="col-md-4">
    <div class="main-service-box mt-20 text-center">
        <i class="fas fa-gift grandient-blue-th-text-color"></i>
        <a href="#"> <h4> Responsive </h4> </a>
        <p>Integer purus ipsum, auctor vitae posuere et, consectetur ac leo. Pellentesque sit amet risus sagittis, fermentum ligula.</p>
    </div>
    </div>
	
	</div>
	</div>
	</section>
	 */}
	
	<Faq />
	<Footer/>
</body>
        );
    }
}

export default WordpressHosting;